package uk.co.libertyapps.tweet.fragment;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.Team;
import uk.co.libertyapps.tweet.adapters.CommentaryAdapter;
import uk.co.libertyapps.tweet.sugar.Commentary;

public class CommentaryFragment extends Fragment {
    public static List<Commentary> commentaryList;
    FilterQueryProvider provider;
    MatrixCursor c;
    public static Spinner spinner;
    CommentaryAdapter a;
    public static String[] arrayItems;
    final int[] actualValues={1001,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62};
    ListView list;
    String[] from = { "script"};
    int[] to = { android.R.id.text1  };
    int id;

    public CommentaryFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_commentary, container, false);

        list = view.findViewById(R.id.list);
        TextView textEmptyCom = view.findViewById(R.id.textEmptyCom);
        list.setEmptyView(textEmptyCom);



        a = new CommentaryAdapter(getActivity(), R.layout.list_com, c, from, to);
        a.setStringConversionColumn(1);

        provider = new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                // run in the background thread
               // Log.d("TAG", "runQuery constraint: " + constraint);
                if (constraint == null) {
                    return null;
                }
                if (constraint.length() < 1) {
                    return null;
                }
                String[] columnNames = { BaseColumns._ID, "SCRIPT" };
                c = new MatrixCursor(columnNames);
                try {
                    // List<Commentary> commentaryList = Commentary.find(Commentary.class, "id = ? ", String.valueOf(constraint));
                    // Log.d("List<Commentary>", " " + commentaryList.size());

                    //       String query = "SELECT * FROM COMMENTARY where SCRIPT LIKE '%" + constraint + "%' LIMIT 100";

                    String query = "SELECT * FROM COMMENTARY where EVENT = '" + constraint + "' LIMIT 100";

                    commentaryList = Commentary.findWithQuery(Commentary.class, query);

                    // todo replace tags

                    for (int i = 0; i < commentaryList.size(); i++) {
                        c.newRow().add(i).add( "" + Team.fixCom(commentaryList.get(i).script));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return c;
            }
        };
        a.setFilterQueryProvider(provider);

        spinner = view.findViewById(R.id.planets_spinner);


        ArrayAdapter<String> SpinerAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, Constants.EVENT_ITEMS);
        spinner.setAdapter(SpinerAdapter);
        spinner.setSelection(0,false);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                String thePrice = String.valueOf(actualValues[ arg2 ]);
                Log.d("thePrice", thePrice);

                provider.runQuery(thePrice);
                a = new CommentaryAdapter(getActivity(), R.layout.list_com, c, from, to);
                list.setAdapter(a);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        list.setAdapter(a);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        spinner.setSelection(id,false);
    }

    public void setArguments(int arguments) {
        id = arguments;
        Log.e("setArguments ", "id =" + id);
    }
}