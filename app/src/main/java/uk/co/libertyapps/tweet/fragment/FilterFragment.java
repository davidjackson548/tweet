package uk.co.libertyapps.tweet.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.adapters.FilterAdapter;
import uk.co.libertyapps.tweet.R;

public class FilterFragment extends DialogFragment implements FilterAdapter.Listener {

    public static RecyclerView recyclerPendingList;
    public static RecyclerView recyclerView;
    TextView textPreviousDate, textEmptyList, textCurrentDate;
    Button editTeam;
    public static int team = 0, cat = 0;
    List<CustomList> customList = new ArrayList<CustomList>();

    public static FilterFragment newInstance() {
        return new FilterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_team, container, false);

        textEmptyList = view.findViewById(R.id.textEmptyList);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerPendingList = view.findViewById(R.id.recyclerPendingList);
        textPreviousDate = view.findViewById(R.id.textPreviousDate);
        textCurrentDate = view.findViewById(R.id.textCurrentDate);
        editTeam = view.findViewById(R.id.editTeam);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerPendingList.setLayoutManager(new LinearLayoutManager(getActivity()));
        textPreviousDate.setVisibility(View.GONE);
        textCurrentDate.setText("What happened?");
        recyclerPendingList.setVisibility(View.GONE);
        editTeam.setVisibility(View.GONE);
        onSuccess();

        return view;
    }

    void createList(String[] idArray, String[] ValueArray) {
        for (int i = 0; i < ValueArray.length; i++) {
            CustomList row = new CustomList();
            row.mobile = idArray[i];
            row.name = ValueArray[i];
            customList.add(i, row);
        }
    }

    public void onSuccess() {
            if (cat == Constants.CAT_ATTEMPTS) {
                String[] idArray = {String.valueOf(Constants.CAT_ATTEMPTS_SHOT), String.valueOf(Constants.CAT_ATTEMPTS_HEAD), String.valueOf(Constants.CAT_ATTEMPTS_VOLLEY)};
                String[] ValueArray = {"Shot","Header","Volley"};
                createList(idArray, ValueArray);
            }

            if (cat == Constants.CAT_ATTEMPTS_SHOT) {
                String[] idArray = {"1","7","4","11"};
                String[] ValueArray = {"On target","Over","Wide","Woodwork"};
                createList(idArray, ValueArray);
            }

            if (cat == Constants.CAT_ATTEMPTS_HEAD) {
                String[] idArray = {"2","8","4","11"};
                String[] ValueArray = {"On target","Over","Wide","Woodwork"};
                createList(idArray, ValueArray);
            }

            if (cat == Constants.CAT_ATTEMPTS_VOLLEY) {
                String[] idArray = {"3","7","4","11"};
                String[] ValueArray = {"On target","Over","Wide","Woodwork"};
                createList(idArray, ValueArray);
            }

            if (cat == Constants.CAT_SET_PIECES) {
                String[] idArray = {"43","47","57","41","49","56"};
                String[] ValueArray = {"Corner","Free kick","Goal Kick","Kick Off","Penalty","Throw In"};
                createList(idArray, ValueArray);
            }
            if (cat == Constants.CAT_FOULS) {
                String[] idArray = {"21","19","18","58","55","30","20"};
                String[] ValueArray = {"Booking","Foul","Handball","Offside","Physio not", "Physio on", "Sent Off"};
                createList(idArray, ValueArray);
            }
            if (cat == Constants.CAT_DEF) {
                String[] idArray = {"48","61","59","62"};
                String[] ValueArray = {"Tackle","Header","Block","Cleared"};
                createList(idArray, ValueArray);
            }
            if (cat == Constants.CAT_MID) {
                String[] idArray = {"26","27","52","53"};
                String[] ValueArray = {"Cross left","Cross right","Dribble left","Dribble right"};
                createList(idArray, ValueArray);
            }
            if (cat == Constants.CAT_GK) {
                String[] idArray = {"37","40"};
                String[] ValueArray = {"keeper holds ball","keeper tips to safety"};
                createList(idArray, ValueArray);
            }

            FilterAdapter mCustomListAdapter =
                    new FilterAdapter(getActivity(), customList, team, this);
            recyclerView.setAdapter(mCustomListAdapter);

            textEmptyList.setVisibility(View.GONE);
    }

    @Override
    public void setEmptyList(boolean visibility) {
        textEmptyList.setVisibility(visibility ? View.VISIBLE : View.GONE);
        recyclerPendingList.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    public void setArguments(int arguments, int dsad) {
        team = arguments;
        cat = dsad;
       // Log.e("setArguments", "" + team);
    }

}