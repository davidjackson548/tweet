package uk.co.libertyapps.tweet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.orm.SugarContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.libertyapps.tweet.adapters.FragmentPageAdapter;
import uk.co.libertyapps.tweet.fragment.EventFragment;

public class MainActivity extends AppCompatActivity  {
    public static boolean running, sex;
    public static ViewPager2 mViewPager;
    public static final int milli = 1000, secs = 60, halfLength = Constants.HALF_LENGTH, gameLength = Constants.GAME_LENGTH;
    public static Handler handler = new Handler();
    public static String tweet, homeTeamName, awayTeamName, editHomeEmoji, editAwayEmoji, hashtag = "", search = "0", homeSeq = "", awaySeq = "",
            venue = "The Venue", matchReport = "", time = "", activePlayer = "player", secondaryPlayer = "another player", attackingTeam = "", defendingTeam = "";
    public static int minutes = 0, homeGoal = 0, awayGoal = 0, half = 0, seconds = 0, progress = 1, homePens = 0, awayPens = 0,
            homeStart = 0,  homeBench = 0, homeReserve = 0, awayStart = 0, awayBench = 0, awayReserve = 0, extra = 0;
    static FragmentPageAdapter myAdapter;
    public static long startTime;
    public static List<CustomList> homeTeamList, awayTeamList;
    private MyDatabase db;
    public static FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

/*        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "login");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "name");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);*/

        Log.d("MainActivity", "onCreate");
        setContentView(R.layout.activity_main);
        mViewPager = findViewById(R.id.pager);
        myAdapter = new FragmentPageAdapter(getSupportFragmentManager(), getLifecycle());
        mViewPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        mViewPager.setAdapter(myAdapter);
//        mViewPager.setCurrentItem(Constants.EVENT_PAGER_ID);
//        mViewPager.setOffscreenPageLimit(3); // to prevent crash on kickoff tweet
    }

    public static String generateTemplate(String s) {
        String time = String.valueOf(minutes + 1);
        String inputText = "";

        if (half == Constants.PRE_GAME) {
            inputText = s + "\n" + hashtag;
        } else {
            // get injury time
            if (half == Constants.FIRST_HALF && minutes > 44) {
                time = halfLength + "+" + minutes % 44;
            }

            if (half == Constants.SECOND_HALF && minutes > 89) {
                time = gameLength + "+" + minutes % 89;
            }

             if (half == Constants.EXTRA_TIME_FIRST && minutes > 104) {
                time = gameLength + "+" + minutes % 104;
            }
            if (half == Constants.EXTRA_TIME_SECOND && minutes > 119) {
                time = gameLength + "+" + minutes % 119;
            }
            inputText = time + "' " + s + "\n"
                    + editHomeEmoji + " " + homeGoal + "-" + awayGoal + " "
                    + editAwayEmoji + " " + hashtag;
        }
        return inputText;
    }



    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume");
        homeTeamList = Team.loadTeam(this, Constants.HOME_TEAM);
        awayTeamList = Team.loadTeam(this, Constants.AWAY_TEAM);

        if (homeTeamList.size() > 0) {
            CustomList.order(homeTeamList);

            homeStart = 0;
            Pattern pattern = Pattern.compile("status=1", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(homeTeamList.toString());
            while (matcher.find()) {
                homeStart++;
            }
        //    Log.d("homeStart", String.valueOf(homeStart));

            homeBench = 0;
            pattern = Pattern.compile("status=2", Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(homeTeamList.toString());
            while (matcher.find()) {
                homeBench++;
            }

       //     Log.d("homeBench", String.valueOf(homeBench));

            homeReserve = 0;
            pattern = Pattern.compile("status=3", Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(homeTeamList.toString());
            while (matcher.find()) {
                homeReserve++;
            }
       //     Log.d("homeReserve", String.valueOf(homeReserve));
        }

        if (awayTeamList.size() > 0) {
            CustomList.order(awayTeamList);

            awayStart = 0;
            Pattern pattern = Pattern.compile("status=1", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(awayTeamList.toString());
            while (matcher.find()) {
                awayStart++;
            }
      //      Log.d("awayStart", String.valueOf(awayStart));

            awayBench = 0;
            pattern = Pattern.compile("status=2", Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(awayTeamList.toString());
            while (matcher.find()) {
                awayBench++;
            }

       //     Log.d("awayBench", String.valueOf(awayBench));

            awayReserve = 0;
            pattern = Pattern.compile("status=3", Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(awayTeamList.toString());
            while (matcher.find()) {
                awayReserve++;
            }
        //    Log.d("awayReserve", String.valueOf(awayReserve));

        }

         // Log.d("homeTeamList", String.valueOf(MainActivity.homeTeamList.toString()));

        SharedPreferences sharedpreferences = getSharedPreferences("match", Context.MODE_MULTI_PROCESS);

        if (sharedpreferences != null ) {
            tweet = sharedpreferences.getString("tweet", "");
            sex =  sharedpreferences.getBoolean("sex", false);
             Log.d("sex", String.valueOf(sex));
            EventFragment.homeShot = sharedpreferences.getInt("homeShot", 0);
            EventFragment.awayShot = sharedpreferences.getInt("awayShot", 0);
            EventFragment.homeFoul = sharedpreferences.getInt("homeFoul", 0);
            EventFragment.awayFoul = sharedpreferences.getInt("awayFoul", 0);
            EventFragment.homeOffside = sharedpreferences.getInt("homeOffside", 0);
            EventFragment.awayOffside = sharedpreferences.getInt("awayOffside", 0);
            homeGoal = sharedpreferences.getInt("homeGoal", 0);
            awayGoal = sharedpreferences.getInt("awayGoal", 0);
            EventFragment.homeCorner = sharedpreferences.getInt("homeCorner", 0);
            EventFragment.awayCorner = sharedpreferences.getInt("awayCorner", 0);

            EventFragment.homeCross = sharedpreferences.getInt("homeCross",0);
            EventFragment.awayCross = sharedpreferences.getInt("awayCross", 0);
            EventFragment.homeDribble = sharedpreferences.getInt("homeDribble", 0);
            EventFragment.awayDribble = sharedpreferences.getInt("awayDribble",0);
            EventFragment.homeTackle = sharedpreferences.getInt("homeTackle", 0);
            EventFragment.awayTackle = sharedpreferences.getInt("awayTackle", 0);
            EventFragment.homeSave = sharedpreferences.getInt("homeSave", 0);
            EventFragment.awaySave = sharedpreferences.getInt("awaySave", 0);
            EventFragment.homeSub = sharedpreferences.getInt("homeSub", 0);
            EventFragment.awaySub = sharedpreferences.getInt("awaySub", 0);

            homePens = sharedpreferences.getInt("homePens", 0);
            awayPens = sharedpreferences.getInt("awayPens", 0);
            homeSeq = sharedpreferences.getString("homeSeq", "");
            awaySeq = sharedpreferences.getString("awaySeq", "");
            matchReport = sharedpreferences.getString("matchReport", "");

            activePlayer = sharedpreferences.getString("activePlayer", "");

            startTime = sharedpreferences.getLong("startTime", 0);

            // Log.d("startTime", "onResume " + startTime);

            running = sharedpreferences.getBoolean("startActive", false);
            handler.removeCallbacksAndMessages(null);
            half = sharedpreferences.getInt("half", 0);
            extra = sharedpreferences.getInt("extra", 0);
            Log.d("EXTRA", String.valueOf(extra));
            progress = sharedpreferences.getInt("progress", 1);

            //  start timer at the correct point
            Long difference = System.currentTimeMillis() - startTime;
            seconds = (int) (difference / milli);

            if (half == Constants.SECOND_HALF) {
                int second_half = halfLength * secs;
                //Log.d("SECOND_HALF", "onResume " + second_half);
                seconds = seconds + second_half;
            }

            if (half == Constants.EXTRA_TIME_FIRST) {
                int extra = gameLength * secs;
                // Log.d("EXTRA_TIME_FIRST", "onResume " + extra);
                seconds = seconds + extra;
            }

            if (half == Constants.EXTRA_TIME_SECOND) {
                int extra = (gameLength + Constants.EXTRA_TIME_HALF_LENGTH) * secs;
                // Log.d("EXTRA_TIME_SECOND", "onResume " + extra);
                seconds = seconds + extra;
            }

            if (running) {
                EventFragment.runTimer();
            }
        }

        db = new MyDatabase(MainActivity.this);
        db.getReadableDatabase();

        SugarContext.init(MainActivity.this);

            hashtag = sharedpreferences.getString("hashtag", "");
            venue = sharedpreferences.getString("editVenue", "");

            SharedPreferences home = getSharedPreferences("home", Context.MODE_MULTI_PROCESS);
            homeTeamName = home.getString("editHomeTeamName", "Home Team");
            editHomeEmoji = home.getString("editHomeEmoji", "");
            attackingTeam = homeTeamName;

      //      Log.d("onResume homeTeamName", homeTeamName);

            SharedPreferences away = getSharedPreferences("away", Context.MODE_MULTI_PROCESS);
            awayTeamName = away.getString("editAwayTeamName", "Away Team");
            editAwayEmoji = away.getString("editAwayEmoji", "");
      //      Log.d("onResume awayTeamName", awayTeamName);

        defendingTeam = awayTeamName;

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop");
        SharedPreferences sharedh = getSharedPreferences("home", Context.MODE_PRIVATE);
        sharedh.edit().putString("editHomeList", MainActivity.homeTeamList.toString()).apply();
      //  Log.d("editHomeList", MainActivity.homeTeamList.toString());

        SharedPreferences shareda = getSharedPreferences("away", Context.MODE_PRIVATE);
        shareda.edit().putString("editAwayList", MainActivity.awayTeamList.toString()).apply();
     //   Log.d("editAwayList", MainActivity.awayTeamList.toString());

        SharedPreferences sharedpreferences = getSharedPreferences("match", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (tweet != null) {
            editor.putString("tweet", tweet);
        }

        // event stats
        editor.putInt("homeShot", EventFragment.homeShot);
        editor.putInt("awayShot", EventFragment.awayShot);
        editor.putInt("homeFoul", EventFragment.homeFoul);
        editor.putInt("awayFoul", EventFragment.awayFoul);
        editor.putInt("homeOffside", EventFragment.homeOffside);
        editor.putInt("awayOffside", EventFragment.awayOffside);
        editor.putInt("homeGoal", MainActivity.homeGoal);
        editor.putInt("awayGoal", MainActivity.awayGoal);
        editor.putInt("homeCorner", EventFragment.homeCorner);
        editor.putInt("awayCorner", EventFragment.awayCorner);
        editor.putInt("homeCross", EventFragment.homeCross);
        editor.putInt("awayCross", EventFragment.awayCross);
        editor.putInt("homeDribble", EventFragment.homeDribble);
        editor.putInt("awayDribble", EventFragment.awayDribble);
        editor.putInt("homeTackle", EventFragment.homeTackle);
        editor.putInt("awayTackle", EventFragment.awayTackle);
        editor.putInt("homeSave", EventFragment.homeSave);
        editor.putInt("awaySave", EventFragment.awaySave);
        editor.putInt("homeSub", EventFragment.homeSub);
        editor.putInt("awaySub", EventFragment.awaySub);
        editor.putInt("homePens", homePens);
        editor.putInt("awayPens", awayPens);
        editor.putString("homeSeq", homeSeq);
        editor.putString("awaySeq", awaySeq);
        editor.putString("matchReport", matchReport);

        if (EventFragment.CheckedTextView1 != null) {
            editor.putBoolean("CheckedTextView1", EventFragment.CheckedTextView1.isChecked());
            editor.putBoolean("CheckedTextView2", EventFragment.CheckedTextView2.isChecked());
            editor.putBoolean("CheckedTextView3", EventFragment.CheckedTextView3.isChecked());
            editor.putBoolean("CheckedTextView4", EventFragment.CheckedTextView4.isChecked());
            editor.putBoolean("CheckedTextView5", EventFragment.CheckedTextView5.isChecked());
            editor.putBoolean("CheckedTextView6", EventFragment.CheckedTextView6.isChecked());
            editor.putBoolean("CheckedTextView7", EventFragment.CheckedTextView7.isChecked());
        }


        // settings
        editor.putInt("half", half);
        editor.putInt("progress", progress);
        editor.putLong("startTime", startTime);

        editor.putBoolean("startActive", running);
        editor.putString("activePlayer", activePlayer);


        editor.commit();

        SugarContext.terminate();
        db.close();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("OAR onActivityResult", "onActivityResult");
        Log.d("OAR requestCode", String.valueOf(requestCode));
        Log.d("OAR resultCode", String.valueOf(resultCode));

        if (resultCode == RESULT_OK) {
            Log.d("OAR onActivityResult", "RESULT_OK");


        }
    }
}
