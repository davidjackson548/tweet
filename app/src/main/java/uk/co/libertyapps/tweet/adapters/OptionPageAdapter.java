package uk.co.libertyapps.tweet.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import uk.co.libertyapps.tweet.fragment.FirstFragment;
import uk.co.libertyapps.tweet.fragment.SecondFragment;
import uk.co.libertyapps.tweet.fragment.ThirdFragment;


public class OptionPageAdapter extends FragmentStateAdapter {

    public OptionPageAdapter(@NonNull FragmentManager fragmentManager, Lifecycle behavior) {
        super(fragmentManager, behavior);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new FirstFragment();
            case 1:
                return new SecondFragment();
            case 2:
                return new ThirdFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
