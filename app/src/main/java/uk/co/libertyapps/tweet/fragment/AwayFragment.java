package uk.co.libertyapps.tweet.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.OptionActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.adapters.StartingAdapter;

public class AwayFragment extends Fragment implements StartingAdapter.Listener {

    RecyclerView recyclerView;
    TextView textEmptyList, textCurrentDate;
    Button editTeam;
    public static StartingAdapter mCustomListAdapter;

    public static AwayFragment newInstance() {
        return new AwayFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_team, container, false);
        textEmptyList = view.findViewById(R.id.textEmptyList);
        recyclerView = view.findViewById(R.id.recyclerView);
        textCurrentDate = view.findViewById(R.id.textCurrentDate);
        editTeam = view.findViewById(R.id.editTeam);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        textCurrentDate.setText("Which " + MainActivity.awayTeamName + " player?");

        editTeam.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), OptionActivity.class);
            intent.putExtra("Team", Constants.AWAY_TEAM);
            startActivity(intent);
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        onSuccess();
    }

    public void onSuccess() {
        try {
            if (MainActivity.awayTeamList.size() > 0) {
                textEmptyList.setVisibility(View.GONE);
         /*   } else {

                CustomList row = new CustomList();
                row.mobile = "0";
                row.name = MainActivity.awayTeamName + " player";
                row.status = Integer.parseInt("1");

                MainActivity.awayTeamList.add(0, row);*/
            }
            mCustomListAdapter =
                    new StartingAdapter(getActivity(), MainActivity.awayTeamList, this, Constants.AWAY_TEAM, Constants.STARTING);
            recyclerView.setAdapter(mCustomListAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setEmptyList(boolean visibility) {
        textEmptyList.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }
}