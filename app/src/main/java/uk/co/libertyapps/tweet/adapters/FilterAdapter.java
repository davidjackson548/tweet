package uk.co.libertyapps.tweet.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.fragment.AwayFragment;
import uk.co.libertyapps.tweet.fragment.EventFragment;
import uk.co.libertyapps.tweet.fragment.FilterFragment;
import uk.co.libertyapps.tweet.fragment.HomeFragment;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.CustomListViewHolder> {

    Context mContext;
    List<CustomList> mCustomList;
    Listener mListener;
    int mTeam;
    public FilterAdapter(Context context, List<CustomList> customList,
                         int team, Listener listener) {
        this.mCustomList = customList;
        this.mContext = context;
        this.mListener = listener;
        this.mTeam = team;
    }

    @Override
    public CustomListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_row, parent, false);
        return new CustomListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomListViewHolder holder, int position) {
        CustomList customList = mCustomList.get(position);

        holder.textName.setText(customList.name);
        holder.textMobile.setText(customList.mobile);

/*        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
             //   EventFragment.filterFragment.cat = 9;
                MainActivity.search = "8";
                MainActivity.mViewPager.setCurrentItem(0);
                return true;
            }
    });*/

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterFragment filterFragment = null;
                MainActivity.search = customList.mobile;
                Log.d("ID", customList.mobile);
                EventFragment.filterFragment.dismiss();
                int pager = 1;
                Fragment frag = null;

                if (mTeam == Constants.HOME_TEAM) {
                    pager = Constants.HOME_PAGER_ID;
                    frag = new HomeFragment();
                } else if (mTeam == Constants.AWAY_TEAM) {
                    pager = Constants.AWAY_PAGER_ID;
                    frag = new AwayFragment();
                }

                //set sub cats to go to pager
                if (EventFragment.filterFragment.cat == Constants.CAT_ATTEMPTS) {
                   // MainActivity.mViewPager.setCurrentItem(pager);
                    //todo how to launch new
                    FragmentManager fm = ((FragmentActivity) mContext).getSupportFragmentManager();
                    EventFragment.secondFilter = FilterFragment.newInstance();
                    EventFragment.secondFilter.setArguments(mTeam, Integer.parseInt(customList.mobile));
                    EventFragment.secondFilter.show(fm, "fragment_filter");
                } else {


                    FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.addToBackStack(null);
                    transaction.setReorderingAllowed(true);
                    transaction.replace(R.id.eventFrag, frag, null);
                    transaction.commit();


            //        MainActivity.mViewPager.setCurrentItem(pager);
                    if ( EventFragment.secondFilter != null) {
                        EventFragment.secondFilter.dismiss();
                    }
                }

               /* if (EventFragment.filterFragment.cat == Constants.CAT_SET_PIECES) {
                    MainActivity.mViewPager.setCurrentItem(pager);
                }

                if (EventFragment.filterFragment.cat == Constants.CAT_FOULS) {
                    MainActivity.mViewPager.setCurrentItem(pager);
                }

                if (EventFragment.filterFragment.cat == Constants.CAT_DEF) {
                    MainActivity.mViewPager.setCurrentItem(pager);
                }

                if (EventFragment.filterFragment.cat == Constants.CAT_MID) {
                    MainActivity.mViewPager.setCurrentItem(pager);
                }*/
            }
        });

        holder.cardView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mCustomList.size();
    }

    public List<CustomList> getCustomList() {
        return mCustomList;
    }

    public void updateCustomList(List<CustomList> customList) {
        this.mCustomList = customList;
    }

    public interface Listener {
        void setEmptyList(boolean visibility);
    }

    public class CustomListViewHolder extends RecyclerView.ViewHolder {

      //  @Bind(R.id.textName)
        TextView textName;

      //  @Bind(R.id.textMobile)
        TextView textMobile;

       // @Bind(R.id.cardView)
        CardView cardView;

        public CustomListViewHolder(View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.textName);
            textMobile = itemView.findViewById(R.id.textMobile);
            cardView = itemView.findViewById(R.id.cardView);
        }

    }
}
