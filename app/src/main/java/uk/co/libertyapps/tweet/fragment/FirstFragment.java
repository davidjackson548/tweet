package uk.co.libertyapps.tweet.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.OptionActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.Team;
import uk.co.libertyapps.tweet.adapters.SelectionAdapter;
import uk.co.libertyapps.tweet.adapters.StartingAdapter;

public class FirstFragment extends Fragment {
    int SELECT_OCR_PICTURE = 200;
    public static EditText editLineUp, editTeamName, editTwitter, editEmoji;
    ImageView teamLogo;
    TextView textEmptyList;
    int tab = 99;
    final String teamname = "editHomeTeamName", emoji = "editHomeEmoji", twitter =  "editHomeTwitter";
    RecyclerView recyclerView;
    TextView textSquadCount;
    public static SelectionAdapter mCustomListAdapter;
    public static List<CustomList> teamList = new ArrayList<CustomList>();

    public FirstFragment() {

    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("home", Context.MODE_PRIVATE);
        editTeamName = view.findViewById(R.id.editTeamName);
        editEmoji =  view.findViewById(R.id.editEmoji);
        editLineUp =  view.findViewById(R.id.editLineUp);
        editTwitter = view.findViewById(R.id.editTwitter);
        teamLogo = view.findViewById(R.id.teamLogo);
   //     teamGoal = view.findViewById(R.id.teamGoal);
        recyclerView = view.findViewById(R.id.recyclerView);
        textEmptyList = view.findViewById(R.id.textEmptyList);
        textSquadCount = view.findViewById(R.id.textSquadCount);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        editTeamName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                String fa = "fulltime.thefa.com";
                if (s.toString().contains(fa)) {
            //        Team.gatImport(getContext(), Constants.HOME_TEAM, FirstFragment.editLineUp);
                    Team.Content grab = new Team.Content();
                    grab.url = s.toString();
                    grab.mCustomListAdapter = mCustomListAdapter;
                    grab.teamList = teamList;
                    grab.editTeamName = editTeamName;
                    grab.execute();
                    // editTeamName.setText("Fylde");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        editLineUp.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.toString().contains(System.lineSeparator())) {
                   // add row
                    List<CustomList> customList;
                 //   CustomList row = new CustomList();
                    String[] squad = new String[100];
                    if (s.length() != 0) {
                        String input = s.toString();
                        String lineSep = ","; // System.getProperty("line.separator");
                        input = input.toString().replace(System.getProperty("line.separator"), lineSep);
                        input = input.replace("/", lineSep);
                        squad = input.split(lineSep);
                        String name, mobile;
                        customList = new ArrayList<CustomList>();
                        // get the names and numbers
                        for (int i = 0; i < squad.length; i++) {

                            CustomList row = new CustomList();
                            row.mobile = squad[i].replaceAll("([^0-9])", "");

                            if (row.mobile.equals("")) {
                                row.mobile = "0";
                            }
                            if (Integer.parseInt(row.mobile) > 99) {
                                row.mobile = "0";
                            }

                            row.name = squad[i].replaceAll("[\\d\\\\-\\\\+\\\\.\\\\^:,]", "");
                            row.status = 3;
                            customList.add(i, row);
                            teamList.add(row);
                            mCustomListAdapter.notifyDataSetChanged();
                        }
                    }
                    editLineUp.setText("");
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        String OCR = sharedpreferences.getString("editHomeOCR", "");

        teamList = Team.loadTeam(getActivity(), Constants.HOME_TEAM);
        CustomList.orderForSelection(teamList);

        editTeamName.setText(sharedpreferences.getString(teamname, ""));
        editEmoji.setText(sharedpreferences.getString(emoji, ""));
        editLineUp.setText("" + OCR);
        editTwitter.setText(sharedpreferences.getString(twitter, ""));

       Uri uri = Uri.parse(sharedpreferences.getString("editHomeLogo", ""));
       Log.d("uri", uri.toString());
       if (uri.toString().length() > 0) {
           Bitmap bitmap1 = BitmapFactory.decodeFile(String.valueOf(uri));
           teamLogo.setImageBitmap(bitmap1);
       }

        teamLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Open", "teamLogo");
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Home Logo"), OptionActivity.SELECT_HOME);
            }
        });



        view.findViewById(R.id.ocrTeam).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("FF", "ocrTeam");
                OptionActivity.team = tab;
                Log.d("FF", "team ocrTeam = " + OptionActivity.team);
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_OCR_PICTURE);
            }
        });

        view.findViewById(R.id.saveTeam).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Team.compareSquads(getContext(), Constants.HOME_TEAM, teamList, editTeamName.getText().toString());
            }
        });

        try {
            if (teamList.size() > 0) {
                textEmptyList.setVisibility(View.GONE);
            }
            mCustomListAdapter =
                    new SelectionAdapter(getActivity(),  teamList, null, Constants.HOME_TEAM, 0);
            recyclerView.setAdapter(mCustomListAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("FF", "onSaveInstanceState");
        List<CustomList> customList;

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("home", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        if (editLineUp.getText().toString() != null) {
            String s = editLineUp.getText().toString();
            String[] squad = new String[100];
            if (s.length() != 0) {

                String lineSep = ","; // System.getProperty("line.separator");
                s = s.replace(System.getProperty("line.separator"), lineSep);
                s = s.replace("/", lineSep);
                squad = s.split(lineSep);
                String name, mobile;
                customList = new ArrayList<CustomList>();
                // get the names and numbers
                for (int i = 0; i < squad.length; i++) {

                    CustomList row = new CustomList();
                    row.mobile = squad[i].replaceAll("([^0-9])", "");

                    if (row.mobile.equals("")) {
                        row.mobile = "0";
                    }
                    if (Integer.parseInt(row.mobile) > 99) {
                        row.mobile = "0";
                    }

                    row.name = squad[i].replaceAll("[\\d\\\\-\\\\+\\\\.\\\\^:,]", "");
                    row.status = 3;
                    customList.add(i, row);
                }

                // save as editHomeList
              editor.putString("editHomeList", customList.toString() + teamList.toString());
            } else {
                editor.putString("editHomeList", teamList.toString());
            }
          //  Log.d("editHomeList", teamList.toString());
            editor.putString("editHomeOCR", "");
        }

        if (editTeamName.getText().toString() != null) {
            editor.putString(teamname,  editTeamName.getText().toString());
        }

        if (editTwitter.getText().toString() != null) {
            editor.putString(twitter, editTwitter.getText().toString());
        }

        if (editEmoji.getText().toString() != null) {
            editor.putString(emoji, editEmoji.getText().toString());
        }

        editor.apply();
    }
}