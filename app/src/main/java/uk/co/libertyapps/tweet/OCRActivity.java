package uk.co.libertyapps.tweet;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.util.Log;

import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class OCRActivity extends AppCompatActivity {
    int team = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendImage(intent); // Handle single image being sent
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendMultipleImages(intent); // Handle multiple images being sent
            }
        }

        if (getIntent().hasExtra("Team")) {
            team = getIntent().getIntExtra("Team", 0);
            Log.d("getOCR", String.valueOf(team));
        }
    }

    private void runTextRecognition(final Bitmap bmp, final int part, final int id, final int position, final Bitmap orginalImage) {
        // Log.d("runTextRecognition", "id = " + id);

        InputImage image = InputImage.fromBitmap(bmp, 0);
        TextRecognizer recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
        recognizer.process(image)
                .addOnSuccessListener(
                        new OnSuccessListener<Text>() {
                            @Override
                            public void onSuccess(Text texts) {
                                // Log.d("onSuccess", "id = " + id);
                                processTextRecognitionResult(texts, part, id, position, orginalImage);
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Task failed with an exception
                                Log.d("onFailure", "id = " + id);
                                e.printStackTrace();
                            }
                        });
    }

    private void processTextRecognitionResult(Text texts, int part, int id, int position, Bitmap orginalImage) {
        String row = "";
        String all = "";

        // Log.d("PTRR id", String.valueOf(id));

        List<Text.TextBlock> blocks = texts.getTextBlocks();
        if (blocks.size() == 0) {
            // ("No text found");
            return;
        }

        for (int i = 0; i < blocks.size(); i++) {
            List<Text.Line> lines = blocks.get(i).getLines();
            // Log.d("lines" + i, String.valueOf(blocks.get(i).getText()));
           // all += String.valueOf(blocks.get(i).getText()) + ' ';
           // all = all.replace("\n", "").replace("\r", "");

            // Log.d("all", all);

            for (int j = 0; j < lines.size(); j++) {
                List<Text.Element> elements = lines.get(j).getElements();

                row += lines.get(j).getText() + System.getProperty("line.separator");
                // Log.d("Row", lines.get(j).getText());
            }
        }

        row = row.replaceAll("GK", "")
                .replaceAll("\\(C\\)", "")
                .replaceAll("Subs", "")
                .replaceAll("Substitute", "")
                .replaceAll(" V ", "")
                .replaceAll(" Vs ", "");


                Log.d("PTRR", "loop done");

        Log.d("row", row);
        Intent intent = new Intent(OCRActivity.this, OptionActivity.class);
        intent.putExtra("row", WordUtils.capitalizeFully(row));
        intent.putExtra("Team", team);
        startActivity(intent);

    }

    void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            // Update UI to reflect image being shared
            Log.d("handleSendImage", imageUri.toString());
            Bitmap bmp = null;
            try {
                bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);

                //todo move filter here
                float[] colorTransform =
                        {       0, 0, 1, 0, 0, // R color
                                0, 0, 1, 0, 0, // G color
                                0, 0, 1, 0, 0, // B color
                                0, 0, 0, 1, 0
                        };

                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.set(colorTransform);
                ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);

                Paint paint = new Paint();
                paint.setColorFilter(colorFilter);

                Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
                Canvas canvas = new Canvas(mutableBitmap);
                canvas.drawBitmap(mutableBitmap, 0, 0, paint);

                Bitmap resultBitmap = Bitmap.createBitmap(mutableBitmap, 0, 0, bmp.getWidth(), bmp.getHeight());

                runTextRecognition(resultBitmap, 0, 0, 0, resultBitmap) ;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            // Log.d("hSMI", imageUris.toString());
            Log.d("hSMI size", String.valueOf(imageUris.size()));

            for (int i = 0; i < imageUris.size(); i++) {

                Log.d("hSMI " + i, "LOOP");
                Bitmap bmp = null;
                try {
                    bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUris.get(i));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //todo move filter here
                float[] colorTransform =
                        {0, 0, 1, 0, 0, // R color
                                0, 0, 1, 0, 0, // G color
                                0, 0, 1, 0, 0, // B color
                                0, 0, 0, 1, 0
                        };

                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.set(colorTransform);

                ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);

                Paint paint = new Paint();
                paint.setColorFilter(colorFilter);

                Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);

                Canvas canvas = new Canvas(mutableBitmap);
                canvas.drawBitmap(mutableBitmap, 0, 0, paint);

                Bitmap resultBitmap = Bitmap.createBitmap(mutableBitmap, 0, 0, bmp.getWidth(), bmp.getHeight());

                Log.d("hSMI " + i, String.valueOf(imageUris.get(i)));

                runTextRecognition(resultBitmap, 0, i, 0, resultBitmap);
            }
        }
    }

}