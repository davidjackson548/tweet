package uk.co.libertyapps.tweet.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.fragment.EventFragment;
import uk.co.libertyapps.tweet.fragment.PenFragment;

public class PenAdapter extends RecyclerView.Adapter<PenAdapter.CustomListViewHolder> {

    Context mContext;
    List<CustomList> mCustomList;
    Listener mListener;
    String subMessage = "";

    public PenAdapter(Context context, List<CustomList> customList,
                      Listener listener) {
        this.mCustomList = customList;
        this.mContext = context;
        this.mListener = listener;
    }

    public void shareTwitter(String message) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, message);
        tweetIntent.setType("text/plain");
        mContext.startActivity(tweetIntent);
    }

    @Override
    public CustomListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_row, parent, false);
        return new CustomListViewHolder(view);
    }

    public String getPens(String s) {
                String inputText = s + "\n" +
                        MainActivity.editHomeEmoji + " " + MainActivity.homeSeq + "\n" +
                        MainActivity.editAwayEmoji + " " + MainActivity.awaySeq + "\n"
                        + MainActivity.editHomeEmoji + " " + MainActivity.homePens + "-" + MainActivity.awayPens + " "
                        + MainActivity.editAwayEmoji + " " + MainActivity.hashtag;
            return inputText;
    }

    @Override
    public void onBindViewHolder(CustomListViewHolder holder, int position) {
        CustomList customList = mCustomList.get(position);

        holder.textName.setText(customList.name);
        holder.textMobile.setText(customList.mobile);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    String who = "";
                    if (PenFragment.team == Constants.HOME_TEAM) {
                        who = MainActivity.homeTeamName;
                    }
                    if (PenFragment.team == Constants.AWAY_TEAM) {
                        who = MainActivity.awayTeamName;
                    }

                    // pop up here
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Did " + customList.name + " score?");
                String finalWho = who;
                builder.setPositiveButton("Scored", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        subMessage = "Penalty scored for " + finalWho + " by " + customList.name;

                        if (PenFragment.team == Constants.HOME_TEAM) {
                            MainActivity.homePens++;
                            EventFragment.homepen.setText(String.valueOf(MainActivity.homePens));
                            MainActivity.homeSeq += "✅";
                        }
                        if (PenFragment.team == Constants.AWAY_TEAM) {
                            MainActivity.awayPens++;
                            EventFragment.awaypen.setText(String.valueOf(MainActivity.awayPens));
                            MainActivity.awaySeq += "✅";
                        }

                        shareTwitter(getPens(subMessage));
                        EventFragment.PenDialogFragment.dismiss();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Missed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        subMessage = "Penalty missed for " + finalWho + " by " + customList.name;
                        if (PenFragment.team == Constants.HOME_TEAM) {
                            MainActivity.homeSeq += "❌";
                        }
                        if (PenFragment.team == Constants.AWAY_TEAM) {
                            MainActivity.awaySeq += "❌";
                        }

                        shareTwitter(getPens(subMessage));
                        EventFragment.PenDialogFragment.dismiss();
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        holder.cardView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mCustomList.size();
    }

    public List<CustomList> getCustomList() {
        return mCustomList;
    }

    public void updateCustomList(List<CustomList> customList) {
        this.mCustomList = customList;
    }

    public interface Listener {
        void setEmptyList(boolean visibility);
    }

    public class CustomListViewHolder extends RecyclerView.ViewHolder {
        TextView textName;
        TextView textMobile;
        CardView cardView;

        public CustomListViewHolder(View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.textName);
            textMobile = itemView.findViewById(R.id.textMobile);
            cardView = itemView.findViewById(R.id.cardView);
        }

    }
}
