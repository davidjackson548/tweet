package uk.co.libertyapps.tweet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Date;

import uk.co.libertyapps.tweet.fragment.CommentaryFragment;
import uk.co.libertyapps.tweet.fragment.EventFragment;

public class Game {
    static void getComFrag(Context context) {
        CommentaryFragment cf = new CommentaryFragment();

        FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.setReorderingAllowed(true);
        transaction.replace(R.id.eventFrag, cf, null);
        transaction.commit();

        cf.setArguments(Integer.parseInt(MainActivity.search));
    }

    public static void setProgress(Context context) {
    switch(MainActivity.progress) {

        case 0:
            EventFragment.kickoff.setText("Tweet start of game");
            MainActivity.handler.removeCallbacksAndMessages(null);
            MainActivity.running = false;

            if (MainActivity.extra == 1) {
                MainActivity.progress = 4;
            } else {
                MainActivity.progress = 1;
            }



            MainActivity.half = Constants.PRE_GAME;

      //      MainActivity.mViewPager.setCurrentItem(Constants.COMMENTARY_PAGER_ID);
            MainActivity.search = "33";
      //      CommentaryFragment.spinner.setSelection(Integer.parseInt(MainActivity.search), false);

            getComFrag(context);


            EventFragment.shareMR(context);
            break;

        case 1:
            EventFragment.kickoff.setText("Tweet half time");
            MainActivity.startTime = System.currentTimeMillis();
            MainActivity.seconds = 0;
            MainActivity.running = true;
            EventFragment.runTimer();
            MainActivity.half = Constants.FIRST_HALF;
            MainActivity.progress = 2;

            EventFragment.statsView.setVisibility(View.VISIBLE);
            EventFragment.preView.setVisibility(View.GONE);

            MainActivity.search = "41";
   /*         CommentaryFragment.spinner.setSelection(Integer.parseInt(MainActivity.search), false);
            MainActivity.mViewPager.setCurrentItem(Constants.COMMENTARY_PAGER_ID);*/

            getComFrag(context);

            break;

        case 2:
            EventFragment.kickoff.setText("Tweet second half");
            MainActivity.handler.removeCallbacksAndMessages(null);
            MainActivity.running = false;
            MainActivity.half = Constants.HALF_TIME;
            MainActivity.progress = 3;

            MainActivity.search = "32";
            getComFrag(context);

            break;

        case 3:
            EventFragment.kickoff.setText("Tweet for full time");
            MainActivity.half = Constants.SECOND_HALF;
            MainActivity.seconds = Constants.HALF_LENGTH * 60;
            MainActivity.minutes = Constants.HALF_LENGTH; // needed to fix LIV-18
            MainActivity.startTime = System.currentTimeMillis();
            MainActivity.running = true;
            EventFragment.runTimer();
            MainActivity.progress = 0;


            MainActivity.search = "50";
            getComFrag(context);

            break;

        case 4:
            EventFragment.kickoff.setText("Tweet start of extra time");
            MainActivity.handler.removeCallbacksAndMessages(null);
            MainActivity.running = false;
            MainActivity.progress = 5;
            MainActivity.half = Constants.FULL_TIME;

            MainActivity.search = "33";
            CommentaryFragment.spinner.setSelection(Integer.parseInt(MainActivity.search), false);
            MainActivity.mViewPager.setCurrentItem(Constants.COMMENTARY_PAGER_ID);

            break;

        case 5:
            EventFragment.kickoff.setText("Tweet extra time half time");
            MainActivity.startTime = System.currentTimeMillis();
            MainActivity.seconds = Constants.GAME_LENGTH * 60;
            MainActivity.minutes = Constants.GAME_LENGTH; // needed to fix LIV-18
            MainActivity.running = true;
            EventFragment.runTimer();
            MainActivity.half = Constants.EXTRA_TIME_FIRST;
            MainActivity.progress = 6;

            EventFragment.statsView.setVisibility(View.VISIBLE);
            EventFragment.preView.setVisibility(View.GONE);

            EventFragment.shareTwitter(MainActivity.generateTemplate("kick off in extra time"), context);
            CommentaryFragment.spinner.setSelection(Integer.parseInt(MainActivity.search), false);
         //   MainActivity.mViewPager.setCurrentItem(Constants.COMMENTARY_PAGER_ID);

            break;

        case 6:
            EventFragment.kickoff.setText("Tweet extra time second half");
            MainActivity.handler.removeCallbacksAndMessages(null);
            MainActivity.running = false;
            MainActivity.half = Constants.EXTRA_HALF_TIME;
            MainActivity.progress = 7;

        //    MainActivity.mViewPager.setCurrentItem(Constants.COMMENTARY_PAGER_ID);
            CommentaryFragment.spinner.setSelection(Integer.parseInt(MainActivity.search), false);

            ImageCreation.createScoreImage(context, Constants.FIRST_HALF, "half time in extra time");

            break;

        case 7:
            EventFragment.kickoff.setText("Tweet extra time full time");
            MainActivity.half = Constants.EXTRA_TIME_SECOND;
            MainActivity.seconds = 105 * 60;
            MainActivity.minutes = 105; // needed to fix LIV-18
            MainActivity.startTime = System.currentTimeMillis();
            MainActivity.running = true;
            EventFragment.runTimer();

            MainActivity.progress = 0;
            EventFragment.shareTwitter(MainActivity.generateTemplate("The second half in extra time is underway"), context);

            break;

        default:
            break;
    }

}

    public static void startTimer() {
        MainActivity.handler.post(new Runnable() {
            @Override

            public void run()
            {
                MainActivity.minutes = (MainActivity.seconds / MainActivity.secs); // / 60;
                int sectime = MainActivity.seconds % MainActivity.secs;
                MainActivity.time = MainActivity.minutes + ":" + String.format("%02d", sectime);

                // Set the text view text.
                // timeView.setText(MainActivity.time);
                if (EventFragment.timeView != null) {
                    EventFragment.timeView.setText(MainActivity.time);
                }
                MainActivity.seconds++;

                // Post the code again with a delay of 1 second.
                MainActivity.handler.postDelayed(this, 1000);
            }
        });
    }

    public static void setupTimerDialog(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        final EditText et = new EditText(context);
        et.setHint("New time (For example) 0:00");
        alertDialogBuilder.setView(et);
        alertDialogBuilder.setCancelable(true).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String time = String.valueOf(et.getText());
                // 1) check for :
                String[] input = time.split(":");
                int correctSizeofArray = 2;
                if (input.length == correctSizeofArray) {
                    Log.d("MainActivity.half", String.valueOf(MainActivity.half));
                    Long inputMinutes = Long.valueOf(input[0]);
                    Long inputSeconds = Long.valueOf(input[1]);
                    int newMinutes = (int) (inputMinutes * 1);
                    int newSeconds = (int) (inputSeconds * 1);

                    // 2) subtract from time to get start point
                    Long secondsAgo = new Long(inputSeconds);
                    Long minutesAgo = new Long(inputMinutes);

                    long remove = (minutesAgo * (MainActivity.secs * MainActivity.milli)) + (secondsAgo * MainActivity.milli);
                    int fix = 0;
                    Log.d("1 remove", String.valueOf(remove));

                    // how to work on starting point of second half.
                    if (MainActivity.half == Constants.SECOND_HALF) {
                        fix = newMinutes - Constants.HALF_LENGTH;
                        remove = fix;
                        remove = remove * (MainActivity.secs * MainActivity.milli) + (secondsAgo * MainActivity.milli);
                        Log.d("2 remove", String.valueOf(fix));
                    }

                    if (MainActivity.half == Constants.EXTRA_TIME_FIRST) {
                        fix = newMinutes - Constants.GAME_LENGTH;
                        remove = fix;
                        remove = remove * (MainActivity.secs * MainActivity.milli) + (secondsAgo * MainActivity.milli);
                        Log.d("3 remove", String.valueOf(fix));
                    }

                    if (MainActivity.half == Constants.EXTRA_TIME_SECOND) {
                        fix = newMinutes - (Constants.GAME_LENGTH + Constants.EXTRA_TIME_HALF_LENGTH);
                        remove = fix;
                        remove = remove * (MainActivity.secs * MainActivity.milli) + (secondsAgo * MainActivity.milli);
                        Log.d("4 remove", String.valueOf(fix));
                    }

                    Date date = new Date();
                    Date dateIn_X_MinAgo = new Date(date.getTime() - remove);
                    Log.d("dateIn_X_MinAgo", String.valueOf(dateIn_X_MinAgo));

                    // 3) save
                    MainActivity.startTime = dateIn_X_MinAgo.getTime();
                    MainActivity.seconds = (newMinutes * MainActivity.secs) + newSeconds;
                }
            }

        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
