package uk.co.libertyapps.tweet.fragment;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.Team;
import uk.co.libertyapps.tweet.adapters.FilterAdapter;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.adapters.SelectionAdapter;
import uk.co.libertyapps.tweet.adapters.StartingAdapter;

public class PlayerFragment extends DialogFragment implements FilterAdapter.Listener {
    public static int team = 0, cat = 0;
    List<CustomList> mCustomList;
    int position;
    Spinner spinner;
    public static  ImageView goalImage;
    EditText playerName;
    SelectionAdapter seas;
    Button removePlayer;

    public static PlayerFragment newInstance() {
        return new PlayerFragment();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("PF", "onSaveInstanceState");

        if (spinner != null) {
            // reload // not SAVING to file yet
            seas.getCustomList().get(position).mobile = String.valueOf(spinner.getSelectedItemPosition());
            Log.d("PF Spinner" , String.valueOf(spinner.getSelectedItemPosition()));
        }

        if (playerName != null) {
            seas.getCustomList().get(position).name = String.valueOf(playerName.getText());
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_goal, container, false);
    }

    public void onViewCreated(@NonNull View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        spinner = v.findViewById(R.id.numSpinner);
        goalImage = v.findViewById(R.id.goalImage);
        removePlayer = v.findViewById(R.id.removePlayer);

        SharedPreferences sharedpreferences = getContext().getSharedPreferences("home", Context.MODE_PRIVATE);
     //   Uri uri = Uri.parse(sharedpreferences.getString("editHomeLogo", ""));


        Uri uri = Uri.parse(mCustomList.get(position).getImage());
        if (uri != null) {
            Bitmap bitmap1 = BitmapFactory.decodeFile(String.valueOf(uri));
            goalImage.setImageBitmap(bitmap1);
        }

        //todo set uri for player image



  /*      String destPath = "file:///storage/emulated/0/Android/data/uk.co.libertyapps.tweet/files/2.png";
  //      destPath = v.getContext().getExternalFilesDir(null).getAbsolutePath() + "/20220108_122555.jpg";
 //       Bitmap bitmap1 = BitmapFactory.decodeFile(destPath);

        Uri backgroundAssetUri = FileProvider.getUriForFile(v.getContext(), "uk.co.libertyapps.tweet.provider", new File(destPath));

        ContentResolver cr = v.getContext().getContentResolver();
        InputStream is = null;
        try {
            is = cr.openInputStream(backgroundAssetUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap image = BitmapFactory.decodeStream(is);
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/

        goalImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("onClick p", String.valueOf(position));
                Log.d("onClick t", String.valueOf(team));
                Intent i = new Intent();
                i.putExtra("position", position);
                i.putExtra("team", team);
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Goal Photo"), position);
            }
        });

        String[] actualValues = {"Pick Number","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21",
                "22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43",
                "44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65",
                "66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87",
                "88","89","90","91","92","93","94","95","96","97","98","99"};

        if (mCustomList.get(position).mobile.equals("")) {
            mCustomList.get(position).mobile = "0";
        }

        ArrayAdapter<String> SpinerAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, actualValues);
        spinner.setAdapter(SpinerAdapter);
        spinner.setSelection(Integer.parseInt(mCustomList.get(position).mobile));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                Log.d("onItemSelected", "onItemSelected");

                ((TextView) arg0.getChildAt(0)).setTextColor(Color.BLACK);
                ((TextView) arg0.getChildAt(0)).setTextSize(32);

                mCustomList.get(position).mobile = String.valueOf(arg2);
                seas.notifyDataSetChanged();
                Team.saveTeam(getContext(), team);

                //todo save value on exit?
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {}
        });

        removePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Remove", mCustomList.get(position).name);
                mCustomList.remove(position);
                seas.notifyDataSetChanged();
                getDialog().dismiss();
            }
        });

        playerName = v.findViewById(R.id.playerName);
        playerName.setText(mCustomList.get(position).name);
        playerName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                mCustomList.get(position).name = s.toString();
               seas.notifyDataSetChanged();
               // Log.d("afterTextChanged", MainActivity.homeTeamList.get(position).name);
                Team.saveTeam(getContext(), team);
              //  getDialog().dismiss();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    public void setArguments(List<CustomList> arguments, int teams, int player, int selected) {
        Log.d("setArguments", String.valueOf(teams));
        mCustomList = arguments;
        position = player;
        team = teams;

       if (team == Constants.HOME_TEAM) {
           seas = FirstFragment.mCustomListAdapter;
        }
        if (team == Constants.AWAY_TEAM) {
            seas = SecondFragment.mCustomListAdapter;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


    @Override
    public void setEmptyList(boolean visibility) {

    }
}