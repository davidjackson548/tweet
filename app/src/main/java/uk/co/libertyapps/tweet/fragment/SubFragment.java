package uk.co.libertyapps.tweet.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.adapters.SubListAdapter;
import uk.co.libertyapps.tweet.Team;

public class SubFragment extends DialogFragment implements SubListAdapter.Listener {

    public static RecyclerView recyclerPendingList;
    public static RecyclerView recyclerView;
    public static TextView textPreviousDate, textEmptyList, textCurrentDate;
    Button editTeam;
    public static int team = 0;
    public static int subPos = Constants.SUB_POS;
    List<CustomList> starting = new ArrayList<CustomList>();
    List<CustomList> bench = new ArrayList<CustomList>();
    List<CustomList> teamList = new ArrayList<CustomList>();

    public static SubFragment newInstance() {
        return new SubFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_team, container, false);

        textEmptyList = view.findViewById(R.id.textEmptyList);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerPendingList = view.findViewById(R.id.recyclerPendingList);
        textPreviousDate = view.findViewById(R.id.textPreviousDate);
        textCurrentDate = view.findViewById(R.id.textCurrentDate);
        editTeam = view.findViewById(R.id.editTeam);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerPendingList.setLayoutManager(new LinearLayoutManager(getActivity()));
        textCurrentDate.setVisibility(View.GONE);
        textPreviousDate.setText("Which player is coming on?");
        recyclerView.setVisibility(View.GONE);
        editTeam.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onSuccess();
    }

    public void onSuccess() {
        try {
            if (bench != null) {
                textEmptyList.setVisibility(View.GONE);

                SubListAdapter mCustomListAdapter =
                        new SubListAdapter(getActivity(), starting, this);
                recyclerView.setAdapter(mCustomListAdapter);

                SubListAdapter mYesterdayCustomListAdapter =
                        new SubListAdapter(getActivity(), bench, this);
                recyclerPendingList.setAdapter(mYesterdayCustomListAdapter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setEmptyList(boolean visibility) {
        textEmptyList.setVisibility(visibility ? View.VISIBLE : View.GONE);
        recyclerPendingList.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    public void setArguments(int arguments) {
        team = arguments;
        int start = 0;
        int subs = 0;
//        Log.e("SubFragment team", "" + team);
        if (team == Constants.HOME_TEAM) {
            teamList = MainActivity.homeTeamList;
            start = MainActivity.homeStart;
            subs = MainActivity.homeBench;
        }

        if (team == Constants.AWAY_TEAM) {
            teamList = MainActivity.awayTeamList;
            start = MainActivity.awayStart;
            subs = MainActivity.awayBench;
        }
/*        int minS = Math.min(teamList.size(), Constants.SIZE_OF_LINEUP);
                Log.e("SubFragment minS", "" + minS);*/

        starting.addAll(teamList.subList(0, start));
        Log.e("SubF starting size", "" + starting.size());
        if (subs > 0) {
            bench.addAll(teamList.subList(start, start + subs));
            Log.e("bench size", "" + bench.size());
        }

    }


}