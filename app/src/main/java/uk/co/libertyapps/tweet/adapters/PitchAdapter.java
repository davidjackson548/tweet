package uk.co.libertyapps.tweet.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;

public class PitchAdapter extends RecyclerView.Adapter<PitchAdapter.CustomListViewHolder> {

    Context mContext;
    List<CustomList> mCustomList;
    Listener mListener;
    int team, selected;

    public PitchAdapter(Context context, List<CustomList> customList,
                        Listener listener, int team, int selected) {
        this.mCustomList = customList;
        this.mContext = context;
        this.mListener = listener;
        this.team = team;
        this.selected = selected;
    }

    @Override
    public CustomListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_pitch, parent, false);
        return new CustomListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomListViewHolder holder, int position) {
        CustomList customList = mCustomList.get(position);

        holder.textName.setText(customList.name);
        holder.textMobile.setText(customList.mobile);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("test", customList.name);

                MainActivity.mViewPager.setCurrentItem(Constants.COMMENTARY_PAGER_ID);
                MainActivity.activePlayer = customList.name;
            }
        });

        holder.cardView.setTag(position);
        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d("onLongClick xyz", String.valueOf(position));
                Log.d("selected xyz", String.valueOf(selected));

                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return mCustomList.size();
    }

    public List<CustomList> getCustomList() {
        return mCustomList;
    }

    public void updateCustomList(List<CustomList> customList) {
        this.mCustomList = customList;
    }

    public interface Listener {
        void setEmptyList(boolean visibility);
    }

    public class CustomListViewHolder extends RecyclerView.ViewHolder {
        TextView textName;
        TextView textMobile;
        CardView cardView;

        public CustomListViewHolder(View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.textName);
            textMobile = itemView.findViewById(R.id.textMobile);
            cardView = itemView.findViewById(R.id.cardView);
        }

    }
}
