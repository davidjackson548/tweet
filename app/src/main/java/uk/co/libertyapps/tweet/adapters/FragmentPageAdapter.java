package uk.co.libertyapps.tweet.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.fragment.AwayFragment;
import uk.co.libertyapps.tweet.fragment.CommentaryFragment;
import uk.co.libertyapps.tweet.fragment.EventFragment;
import uk.co.libertyapps.tweet.fragment.HomeFragment;


public class FragmentPageAdapter extends FragmentStateAdapter {

    public FragmentPageAdapter(@NonNull FragmentManager fragmentManager, Lifecycle behavior) {
        super(fragmentManager, behavior);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
/*            case Constants.HOME_PAGER_ID:
                return new HomeFragment();*/
            case 0:
                return new EventFragment();
/*            case Constants.AWAY_PAGER_ID:
                return new AwayFragment();
            case Constants.COMMENTARY_PAGER_ID:
                return new CommentaryFragment();*/
            default:
                break;
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
