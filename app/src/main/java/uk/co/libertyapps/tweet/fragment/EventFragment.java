package uk.co.libertyapps.tweet.fragment;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import java.util.List;

import uk.co.libertyapps.tweet.BuildConfig;
import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.Game;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.OCRActivity;
import uk.co.libertyapps.tweet.OptionActivity;
import uk.co.libertyapps.tweet.R;

public class EventFragment extends Fragment {
//    private View view;
    public static int homeShot = 0, awayShot = 0, homeFoul = 0, awayFoul = 0, homeOffside = 0,
            awayOffside = 0, homeCorner = 0, awayCorner = 0, homeCross = 0, awayCross = 0,
    homeDribble = 0, awayDribble = 0, homeSub = 0, awaySub = 0, homeSave = 0, awaySave = 0,
    homeTackle = 0, awayTackle = 0, homePens = 0, awayPens = 0 ;
    public static TextView version, timeView, homeTeamName, awayTeamName;
    public static Button kickoff, homeShots, awayShots, homeFouls,
    awayFouls, homeOffsides, awayOffsides, homeGoals, awayGoals, homeCorners, awayCorners, homeDribbles, awayDribbles,
            homeCrosses, awayCrosses, homeSubs, awaySubs, homeSaves, awaySaves, homeTackles, awayTackles, homepen, awaypen;
    public static SubFragment SubFragment;
    public static PenFragment PenDialogFragment;
    public static FilterFragment filterFragment, secondFilter;
    public static CheckedTextView CheckedTextView1, CheckedTextView2, CheckedTextView3, CheckedTextView4, CheckedTextView5, CheckedTextView6, CheckedTextView7;
    public static View statsView, preView;

    public static EventFragment newInstance() {
        return new EventFragment();
    }

    public static void shareTwitter(String message, Context context) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, message);
        tweetIntent.setType("text/plain");
        context.startActivity(tweetIntent);
    }

    public static void shareMR(Context context) {
   /*     Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("message/rfc822");
  //      sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"jones27@gmail.com"});
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, MainActivity.homeTeamName + " vs " + MainActivity.awayTeamName);
        sendIntent.putExtra(Intent.EXTRA_TEXT, MainActivity.matchReport);
        context.startActivity(sendIntent);*/
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
    //    Log.d("EventFragment", "onCreateView");
        return inflater.inflate(R.layout.activity_event, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
   //     Log.d("EventFragment", "onViewCreated");
        String kickOffText = "";

        Button demo = view.findViewById(R.id.demo);
        demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = "ESLDudAAo-Q";
                Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
                Intent webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.youtube.com/watch?v=" + id));
                try {
                    startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    startActivity(webIntent);
                }
            }
        });

        Button option = view.findViewById(R.id.option);
        option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), OptionActivity.class);
                intent.putExtra("Team", 3);
                startActivity(intent);
            }
        });

        statsView = view.findViewById(R.id.statsView);
        preView = view.findViewById(R.id.preView);

        homeTeamName = view.findViewById(R.id.homeTeamName);
        awayTeamName = view.findViewById(R.id.awayTeamName);
        homeTeamName.setText(MainActivity.homeTeamName);
        awayTeamName.setText(MainActivity.awayTeamName);
        version = view.findViewById(R.id.version);
        version.setText("Liberty Apps 2021. Version " + BuildConfig.VERSION_NAME);


        timeView  = view.findViewById(R.id.timer);

        kickoff = view.findViewById(R.id.kickoff);
        if (MainActivity.progress == 1) {
            kickOffText = "Tweet start of game";
            statsView.setVisibility(View.GONE);
            preView.setVisibility(View.VISIBLE);
        }
        if (MainActivity.progress == 2) {
            kickOffText = "Tweet half time";
            statsView.setVisibility(View.VISIBLE);
            preView.setVisibility(View.GONE);
        }
        if (MainActivity.progress == 3) {
            kickOffText = "Tweet second half";
            statsView.setVisibility(View.VISIBLE);
            preView.setVisibility(View.GONE);
        }
        if (MainActivity.progress == 0) {
            kickOffText = "Tweet for full time";
            statsView.setVisibility(View.VISIBLE);
            preView.setVisibility(View.GONE);
        }
        if (MainActivity.progress == 4) {
            kickOffText = "Tweet start of extra time";
            statsView.setVisibility(View.GONE);
            preView.setVisibility(View.VISIBLE);
        }
        if (MainActivity.progress == 5) {
            kickOffText = "Tweet extra time half time";
            statsView.setVisibility(View.VISIBLE);
            preView.setVisibility(View.GONE);
        }
        if (MainActivity.progress == 6) {
            kickOffText = "Tweet extra time second half";
            statsView.setVisibility(View.VISIBLE);
            preView.setVisibility(View.GONE);
        }
        if (MainActivity.progress == 7) {
            kickOffText = "Tweet extra time full time";
            statsView.setVisibility(View.VISIBLE);
            preView.setVisibility(View.GONE);
        }
        kickoff.setText(kickOffText);

        homepen = view.findViewById(R.id.homepen);
        awaypen = view.findViewById(R.id.awaypen);
        homepen.setText(String.valueOf(MainActivity.homePens));
        awaypen.setText(String.valueOf(MainActivity.awayPens));

        homeGoals = view.findViewById(R.id.homeGoal);
        awayGoals = view.findViewById(R.id.awayGoal);
        homeGoals.setText(String.valueOf(MainActivity.homeGoal));
        awayGoals.setText(String.valueOf(MainActivity.awayGoal));

        homeShots = view.findViewById(R.id.homeShots);
        awayShots = view.findViewById(R.id.awayShots);
        homeShots.setText(String.valueOf(homeShot));
        awayShots.setText(String.valueOf(awayShot));

        homeFouls = view.findViewById(R.id.homeFoul);
        awayFouls = view.findViewById(R.id.awayFoul);
        homeFouls.setText(String.valueOf(homeFoul));
        awayFouls.setText(String.valueOf(awayFoul));

        homeOffsides = view.findViewById(R.id.homeOffside);
        awayOffsides = view.findViewById(R.id.awayOffside);
        homeOffsides.setText(String.valueOf(homeOffside));
        awayOffsides.setText(String.valueOf(awayOffside));

        homeCorners = view.findViewById(R.id.homeCorner);
        awayCorners = view.findViewById(R.id.awayCorner);
        homeCorners.setText(String.valueOf(homeCorner));
        awayCorners.setText(String.valueOf(awayCorner));

        homeCrosses = view.findViewById(R.id.homeCross);
        awayCrosses = view.findViewById(R.id.awayCross);
        homeCrosses.setText(String.valueOf(homeCross));
        awayCrosses.setText(String.valueOf(awayCross));

        homeDribbles = view.findViewById(R.id.homeDribble);
        awayDribbles = view.findViewById(R.id.awayDribble);
        homeDribbles.setText(String.valueOf(homeDribble));
        awayDribbles.setText(String.valueOf(awayDribble));

        homeSubs = view.findViewById(R.id.homeSub);
        awaySubs = view.findViewById(R.id.awaySub);
        homeSubs.setText(String.valueOf(homeSub));
        awaySubs.setText(String.valueOf(awaySub));

        kickoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Log.d("half", String.valueOf(MainActivity.half));
            Game.setProgress(getActivity());
            }
        });

        homeGoals.setOnLongClickListener(v -> {
            homeShot--;
            homeShots.setText(String.valueOf(homeShot));
            MainActivity.homeGoal--;
            homeGoals.setText(String.valueOf(MainActivity.homeGoal));
            return true;
        });

        awayGoals.setOnLongClickListener(v -> {
            awayShot--;
            awayShots.setText(String.valueOf(awayShot));
            MainActivity.awayGoal--;
            awayGoals.setText(String.valueOf(MainActivity.awayGoal));
            return true;
        });

        homeSaves = view.findViewById(R.id.homeSave);
        awaySaves = view.findViewById(R.id.awaySave);
        homeSaves.setText(String.valueOf(homeSave));
        awaySaves.setText(String.valueOf(awaySave));

        homeTackles = view.findViewById(R.id.homeTackle);
        awayTackles = view.findViewById(R.id.awayTackle);
        homeTackles.setText(String.valueOf(homeTackle));
        awayTackles.setText(String.valueOf(awayTackle));

        homeShots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeShot++;
                homeShots.setText(String.valueOf(homeShot));

                //     MainActivity.mViewPager.setCurrentItem(Constants.HOME_PAGER_ID);
                MainActivity.attackingTeam = MainActivity.homeTeamName;
                MainActivity.defendingTeam = MainActivity.awayTeamName;

                showFilterDialog(Constants.HOME_TEAM, Constants.CAT_ATTEMPTS);
            }
        });

        awayShots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awayShot++;
                awayShots.setText(String.valueOf(awayShot));
                //    MainActivity.mViewPager.setCurrentItem(Constants.AWAY_PAGER_ID);

                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;
                showFilterDialog(Constants.AWAY_TEAM, Constants.CAT_ATTEMPTS);
            }
        });

        homeFouls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFoul++;
                homeFouls.setText(String.valueOf(homeFoul));
                //    MainActivity.mViewPager.setCurrentItem(Constants.HOME_PAGER_ID);
                MainActivity.attackingTeam = MainActivity.homeTeamName;
                MainActivity.defendingTeam = MainActivity.awayTeamName;
                showFilterDialog(Constants.HOME_TEAM, Constants.CAT_FOULS);
                MainActivity.search = "19";
            }
        });

        awayFouls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awayFoul++;
                awayFouls.setText(String.valueOf(awayFoul));
                //    MainActivity.mViewPager.setCurrentItem(Constants.AWAY_PAGER_ID);
                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;
                showFilterDialog(Constants.AWAY_TEAM, Constants.CAT_FOULS);
                MainActivity.search = "19";
            }
        });

        homeOffsides.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // change text here
                homeOffside++;
                homeOffsides.setText(String.valueOf(homeOffside));
                //    MainActivity.mViewPager.setCurrentItem(Constants.HOME_PAGER_ID);

                MainActivity.attackingTeam = MainActivity.homeTeamName;
                MainActivity.defendingTeam = MainActivity.awayTeamName;

                MainActivity.search = "58";
            }
        });

        awayOffsides.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awayOffside++;
                awayOffsides.setText(String.valueOf(awayOffside));
                //     MainActivity.mViewPager.setCurrentItem(Constants.AWAY_PAGER_ID);

                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;

                MainActivity.search = "58";

            }
        });

        homeGoals.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeShot++;
                    homeShots.setText(String.valueOf(homeShot));
                    MainActivity.homeGoal++;
                    homeGoals.setText(String.valueOf(MainActivity.homeGoal));

                    MainActivity.tweet = MainActivity.tweet + "GOAL FOR " + MainActivity.homeTeamName.toUpperCase() + ".\n";
                    // MainActivity.mViewPager.setCurrentItem(Constants.HOME_PAGER_ID);

                    MainActivity.attackingTeam = MainActivity.homeTeamName;
                    MainActivity.defendingTeam = MainActivity.awayTeamName;

                    MainActivity.search = "13";

                    // open frag here
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.addToBackStack(null);
                    transaction.setReorderingAllowed(true);
                    transaction.replace(R.id.eventFrag, new HomeFragment(), null);
                    transaction.commit();
                }
        });

        awayGoals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awayShot++;
                awayShots.setText(String.valueOf(awayShot));
                MainActivity.awayGoal++;
                awayGoals.setText(String.valueOf(MainActivity.awayGoal));

       //         MainActivity.mViewPager.setCurrentItem(Constants.AWAY_PAGER_ID);
                MainActivity.tweet = MainActivity.tweet + "GOAL FOR " + MainActivity.awayTeamName.toUpperCase() + ".\n";

                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;

                MainActivity.search = "13";

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.addToBackStack(null);
                transaction.setReorderingAllowed(true);
                transaction.replace(R.id.eventFrag, new AwayFragment(), null);
                transaction.commit();
            }
        });

        homeCrosses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // change text here
                homeCross++;
                homeCrosses.setText(String.valueOf(homeCross));

                //      MainActivity.mViewPager.setCurrentItem(Constants.HOME_PAGER_ID);
                MainActivity.attackingTeam = MainActivity.homeTeamName;
                MainActivity.defendingTeam = MainActivity.awayTeamName;
                MainActivity.search = "26";
            }
        });

        awayCrosses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awayCross++;
                awayCrosses.setText(String.valueOf(awayCross));

                //       MainActivity.mViewPager.setCurrentItem(Constants.AWAY_PAGER_ID);
                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;
                MainActivity.search = "27";
            }
        });

        homeCorners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeCorner++;
                homeCorners.setText(String.valueOf(homeCorner));

                MainActivity.attackingTeam = MainActivity.homeTeamName;
                MainActivity.defendingTeam = MainActivity.awayTeamName;
                MainActivity.search = "43";
                showFilterDialog(Constants.HOME_TEAM, Constants.CAT_SET_PIECES);
            }
        });

        awayCorners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awayCorner++;
                awayCorners.setText(String.valueOf(awayCorner));

                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;
                MainActivity.search = "43";
                showFilterDialog(Constants.AWAY_TEAM, Constants.CAT_SET_PIECES);
            }
        });

        homeDribbles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeDribble++;
                homeDribbles.setText(String.valueOf(homeDribble));

                MainActivity.attackingTeam = MainActivity.homeTeamName;
                MainActivity.defendingTeam = MainActivity.awayTeamName;
                MainActivity.search = "52";
                showFilterDialog(Constants.HOME_TEAM, Constants.CAT_MID);
            }
        });

        awayDribbles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awayDribble++;
                awayDribbles.setText(String.valueOf(awayDribble));

                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;
                MainActivity.search = "53";
                showFilterDialog(Constants.AWAY_TEAM, Constants.CAT_MID);
            }
        });

        homepen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPenDialog(Constants.HOME_TEAM);
            }
        });

        awaypen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPenDialog(Constants.AWAY_TEAM);
            }
        });

        homeSubs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog(Constants.HOME_TEAM);
            }
        });

        awaySubs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog(Constants.AWAY_TEAM);
            }
        });

        homeSaves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeSave++;
                homeSaves.setText(String.valueOf(homeSave));

         //       MainActivity.mViewPager.setCurrentItem(Constants.HOME_PAGER_ID);
                MainActivity.attackingTeam = MainActivity.homeTeamName;
                MainActivity.defendingTeam = MainActivity.awayTeamName;

                showFilterDialog(Constants.HOME_TEAM, Constants.CAT_GK);
            }
        });

        awaySaves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awaySave++;
                awaySaves.setText(String.valueOf(awaySave));
          //      MainActivity.mViewPager.setCurrentItem(Constants.AWAY_PAGER_ID);

                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;

                showFilterDialog(Constants.AWAY_TEAM, Constants.CAT_GK);
            }
        });

        homeTackles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeTackle++;
                homeTackles.setText(String.valueOf(homeTackle));

                MainActivity.attackingTeam = MainActivity.homeTeamName;
                MainActivity.defendingTeam = MainActivity.awayTeamName;

                showFilterDialog(Constants.HOME_TEAM, Constants.CAT_DEF);
            }
        });

        awayTackles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awayTackle++;
                awayTackles.setText(String.valueOf(awayTackle));

                MainActivity.attackingTeam = MainActivity.awayTeamName;
                MainActivity.defendingTeam = MainActivity.homeTeamName;

                showFilterDialog(Constants.AWAY_TEAM, Constants.CAT_DEF);
            }
        });

        timeView.setOnClickListener(v -> Game.setupTimerDialog(getActivity()));

        CheckedTextView1 = view.findViewById(R.id.checkedTextView1);
        CheckedTextView2 = view.findViewById(R.id.checkedTextView2);
        CheckedTextView3 = view.findViewById(R.id.checkedTextView3);
        CheckedTextView4 = view.findViewById(R.id.checkedTextView4);
        CheckedTextView5 = view.findViewById(R.id.checkedTextView5);
        CheckedTextView6 = view.findViewById(R.id.checkedTextView6);
        CheckedTextView7 = view.findViewById(R.id.checkedTextView7);

        CheckedTextView5.setOnClickListener(v -> {
            MainActivity.tweet = "\uD83E\uDD17 Huddle completed. Time for kick off.";
            shareTwitter();
            CheckedTextView5.setChecked(true);
        });

        CheckedTextView4.setOnClickListener(v -> {
                MainActivity.tweet = "\uD83E\uDD1D Handshake completed.";
                shareTwitter();
                CheckedTextView4.setChecked(true);
        });

        CheckedTextView1.setOnClickListener(v -> {
                MainActivity.tweet = "\uD83D\uDC65 Both sets of players are warming up on the pitch before the big game.";
                shareTwitter();
                CheckedTextView1.setChecked(true);
        });

        CheckedTextView3.setOnClickListener(v -> {
            MainActivity.tweet = "\uD83C\uDFDF️ The players enter the pitch. We are wearing the  kit";
            shareTwitter();
            CheckedTextView3.setChecked(true);
        });

        CheckedTextView6.setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), OptionActivity.class);
                intent.putExtra("Team", Constants.HOME_TEAM);
                startActivity(intent);
                CheckedTextView6.setChecked(true);
        });

        CheckedTextView7.setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), OptionActivity.class);
                intent.putExtra("Team", Constants.AWAY_TEAM);
                startActivity(intent);
                CheckedTextView7.setChecked(true);
        });

        CheckedTextView2.setOnClickListener(v -> {
                MainActivity.tweet = "The temperature is °C.";
                shareTwitter();
                CheckedTextView2.setChecked(true);
        });
    }

    private void showEditDialog(int side) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        SubFragment = SubFragment.newInstance();
        SubFragment.setArguments(side);
        SubFragment.setShowsDialog(true);
        SubFragment.show(fm, "fragment_sub");
    }

    private void showPenDialog(int side) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        PenDialogFragment = PenFragment.newInstance();
        PenDialogFragment.setArguments(side);
        PenDialogFragment.show(fm, "fragment_pen");
    }

    private void showFilterDialog(int side, int cat) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        filterFragment = FilterFragment.newInstance();
        filterFragment.setArguments(side, cat);
        filterFragment.show(fm, "fragment_filter");
    }

    public void shareTwitter() {
        String message = MainActivity.tweet;

        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
        tweetIntent.putExtra(Intent.EXTRA_TEXT, MainActivity.generateTemplate(message));
        tweetIntent.setType("text/plain");

        PackageManager packManager = getActivity().getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent,  PackageManager.MATCH_DEFAULT_ONLY);
        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList){
            if(resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")){
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if (resolved) {
            // launch in app
            getActivity().startActivity(tweetIntent);
        } else {
            // launch in browser
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + message;
            Log.d("tweetUrl", tweetUrl);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(tweetUrl));
            startActivity(i);
        }

        MainActivity.tweet = "";
    }

    public static void runTimer() {
        Game.startTimer();
    }

    @Override
    public void onResume() {
    //    Log.d("EventFragment", "onResume");
        super.onResume();
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("match", Context.MODE_MULTI_PROCESS);
        EventFragment.CheckedTextView1.setChecked(sharedpreferences.getBoolean("CheckedTextView1", false));
        EventFragment.CheckedTextView2.setChecked(sharedpreferences.getBoolean("CheckedTextView2", false));
        EventFragment.CheckedTextView3.setChecked(sharedpreferences.getBoolean("CheckedTextView3", false));
        EventFragment.CheckedTextView4.setChecked(sharedpreferences.getBoolean("CheckedTextView4", false));
        EventFragment.CheckedTextView5.setChecked(sharedpreferences.getBoolean("CheckedTextView5", false));
        EventFragment.CheckedTextView6.setChecked(sharedpreferences.getBoolean("CheckedTextView6", false));
        EventFragment.CheckedTextView7.setChecked(sharedpreferences.getBoolean("CheckedTextView7", false));
    }
}