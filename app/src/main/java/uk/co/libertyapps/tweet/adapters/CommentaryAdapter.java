package uk.co.libertyapps.tweet.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.ImageCreation;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.Team;
import uk.co.libertyapps.tweet.fragment.CommentaryFragment;
import uk.co.libertyapps.tweet.fragment.EventFragment;
import uk.co.libertyapps.tweet.sugar.Commentary;

public class CommentaryAdapter extends SimpleCursorAdapter {
    private Context mContext;

    public CommentaryAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context,layout,c,from,to);
        this.mContext = context;
    }

    public void shareTwitter() {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
        tweetIntent.setType("text/plain");
        tweetIntent.putExtra(Intent.EXTRA_TEXT, MainActivity.generateTemplate(MainActivity.tweet));

        PackageManager packManager = mContext.getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent,  PackageManager.MATCH_DEFAULT_ONLY);
        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList){
            if(resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")){
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if (resolved) {
            // launch in app
            mContext.startActivity(tweetIntent);
        } else {
            // launch in browser
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + MainActivity.generateTemplate(MainActivity.tweet);
            Log.d("tweetUrl", tweetUrl);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(tweetUrl));
            mContext.startActivity(i);
        }
        MainActivity.tweet = "";
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        // Log.d("bindView activePlayer", MainActivity.activePlayer);
        int position = cursor.getPosition();
        Button btnAddCom = view.findViewById(R.id.btnAddCom);
        btnAddCom.setText(cursor.getString(1));

        btnAddCom.setOnLongClickListener(v -> {
            MainActivity.tweet = MainActivity.tweet + Team.fixCom(CommentaryFragment.commentaryList.get(position).script) + " ";

            Toast.makeText(mContext, "Text added", Toast.LENGTH_SHORT).show();
   //         MainActivity.mViewPager.setCurrentItem(Constants.EVENT_PAGER_ID);

            EventFragment ef = new EventFragment();

            FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.setReorderingAllowed(true);
            transaction.replace(R.id.eventFrag, ef, null);
            transaction.commit();

      //      CommentaryFragment.spinner.setSelection(0);

            return true;
        });

        btnAddCom.setOnClickListener(v -> {
            MainActivity.tweet = (MainActivity.tweet +  Team.fixCom(CommentaryFragment.commentaryList.get(position).script));

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, MainActivity.search);
    //        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "name");
    //        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
            MainActivity.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);


            // get scorer here?
            if (MainActivity.search.equals("13") || MainActivity.search.equals("14") ) {
                SharedPreferences sharedpreferences = context.getSharedPreferences("match", Context.MODE_PRIVATE);

                Uri uri = Uri.parse(sharedpreferences.getString("editHomeGoal", ""));
                Log.d("uri", uri.toString());
                if (uri.toString().length() > 0) {
                    Bitmap bitmap1 = BitmapFactory.decodeFile(String.valueOf(uri));
                    ImageCreation.createImageFromBitmap(context, bitmap1, "/goal.jpg", MainActivity.generateTemplate(MainActivity.tweet));
                } else {
                    // Log.d("GOAL", "attach image here");
                    ImageCreation.createImageFromDrawable(context, R.drawable.goalless, "/goal.jpg", MainActivity.generateTemplate(MainActivity.tweet));
                }
            }
            else if (MainActivity.search.equals("32")) {
                ImageCreation.createScoreImage(context, Constants.FIRST_HALF, MainActivity.tweet);
            }
            else if (MainActivity.search.equals("33")) {
                ImageCreation.createScoreImage(context, Constants.SECOND_HALF, MainActivity.tweet);
            } else {
                // no image required
                MainActivity.generateTemplate(MainActivity.tweet);
                shareTwitter();
            }

            MainActivity.tweet = "";
         //   MainActivity.mViewPager.setCurrentItem(Constants.EVENT_PAGER_ID);

            EventFragment ef = new EventFragment();

            FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.setReorderingAllowed(true);
            transaction.replace(R.id.eventFrag, ef, null);
            transaction.commit();

         //   CommentaryFragment.spinner.setSelection(0);
        });

    }

}
