package uk.co.libertyapps.tweet.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.Team;
import uk.co.libertyapps.tweet.fragment.EventFragment;
import uk.co.libertyapps.tweet.fragment.SubFragment;

public class SubListAdapter extends RecyclerView.Adapter<SubListAdapter.CustomListViewHolder> {

    Context mContext;
    List<uk.co.libertyapps.tweet.CustomList> mCustomList;
    Listener mListener;
//    int subPos = -1;
    int replacePos;

    public SubListAdapter(Context context, List<uk.co.libertyapps.tweet.CustomList> customList,
                          Listener listener) {
        this.mCustomList = customList;
        this.mContext = context;
        this.mListener = listener;
    }

    public void shareTwitter(String message) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, message);
        tweetIntent.setType("text/plain");
        mContext.startActivity(tweetIntent);
    }

    @Override
    public CustomListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_row, parent, false);
        return new CustomListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomListViewHolder holder, int position) {
        CustomList customList = mCustomList.get(position);

        holder.textName.setText(customList.name);
        holder.textMobile.setText(customList.mobile);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SubFragment.subPos == -1) {
                    SubFragment.subPos = position + Constants.SIZE_OF_LINEUP;

                    SubFragment.recyclerPendingList.setVisibility(View.GONE);
                    SubFragment.recyclerView.setVisibility(View.VISIBLE);
                    SubFragment.textCurrentDate.setVisibility(View.VISIBLE);
                    SubFragment.textPreviousDate.setVisibility(View.GONE);
                    SubFragment.textCurrentDate.setText("Which player is coming off?");
                } else {
                    replacePos = position;

                    List<CustomList> list = null;
                    String who = "";

                    if (SubFragment.team == Constants.HOME_TEAM) {
                        list =  MainActivity.homeTeamList;
                        who = MainActivity.homeTeamName;
                        EventFragment.homeSub ++;
                       EventFragment.homeSubs.setText(String.valueOf(EventFragment.homeSub));
                    }
                    if (SubFragment.team == Constants.AWAY_TEAM) {
                        list =  MainActivity.awayTeamList;
                        who = MainActivity.awayTeamName;
                        EventFragment.awaySub ++;
                        EventFragment.awaySubs.setText(String.valueOf(EventFragment.awaySub));
                    }

                    list.get(replacePos).status = 2;
                    list.get(SubFragment.subPos).status = 1;

                    // swap players
                    CustomList temp1 = list.get(SubFragment.subPos);
                    CustomList temp2 = list.get(replacePos);
                    list.set(replacePos, temp1);
                    list.set(SubFragment.subPos, temp2);



                    String subMessage = "\uD83D\uDD04 Sub for " + who + ".\n" + list.get(replacePos).name + " replaces " + list.get(SubFragment.subPos).name;

                   //todo player sub images

           //         int in = Integer.parseInt(squad[replacePos].replaceAll("\\D+", "0"));
           //         int out = Integer.parseInt(squad[SubFragment.subPos].replaceAll("\\D+", "0"));

                    // tweet here
               //     if (in > 0 && out > 0) {
               //         ImageCreation.createSubImage(mContext, subMessage, in, out);
               //     } else {
                        // tweet here
                        shareTwitter(MainActivity.generateTemplate(subMessage));
               //     }
                        EventFragment.SubFragment.dismiss();
                        SubFragment.subPos = Constants.SUB_POS;
                    }
                }

        });

        holder.cardView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mCustomList.size();
    }

    public List<CustomList> getCustomList() {
        return mCustomList;
    }

    public void updateCustomList(List<CustomList> customList) {
        this.mCustomList = customList;
    }

    public interface Listener {
        void setEmptyList(boolean visibility);
    }

    public class CustomListViewHolder extends RecyclerView.ViewHolder {

      //  @Bind(R.id.textName)
        TextView textName;

      //  @Bind(R.id.textMobile)
        TextView textMobile;

       // @Bind(R.id.cardView)
        CardView cardView;

        public CustomListViewHolder(View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.textName);
            textMobile = itemView.findViewById(R.id.textMobile);
            cardView = itemView.findViewById(R.id.cardView);
        }

    }
}
