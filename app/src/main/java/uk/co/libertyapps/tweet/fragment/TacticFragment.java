package uk.co.libertyapps.tweet.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.OptionActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.adapters.PitchAdapter;
import uk.co.libertyapps.tweet.adapters.StartingAdapter;

public class TacticFragment extends Fragment implements StartingAdapter.Listener {

    RecyclerView benchView, keeperView, defenderView, midfieldView, forwardView;
    public static TextView textPreviousDate;
    public TextView textCurrentDate;
    Button editTeam;

    public static TacticFragment newInstance() {
        return new TacticFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_formation, container, false);

        // setup 4 positions require 4 layouts
        keeperView = view.findViewById(R.id.keeperView);
        defenderView = view.findViewById(R.id.defenderView);
        midfieldView = view.findViewById(R.id.midfieldView);
        forwardView = view.findViewById(R.id.forwardView);
        benchView = view.findViewById(R.id.benchView);

        textPreviousDate = view.findViewById(R.id.textPreviousDate);
        textCurrentDate = view.findViewById(R.id.textCurrentDate);
        editTeam = view.findViewById(R.id.editTeam);

        keeperView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        defenderView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        midfieldView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        forwardView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        benchView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        textCurrentDate.setText(MainActivity.homeTeamName + " Lineup");
        textPreviousDate.setText(MainActivity.homeTeamName + " Subs");

        editTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ImageCreation.takeScreenShot(getActivity());

                Intent intent = new Intent(getActivity(), OptionActivity.class);
                intent.putExtra("Team", Constants.HOME_TEAM);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
            onSuccess();
    }

    private void onSuccess() {
        try {

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences("home", Context.MODE_PRIVATE);

            int form = sharedpreferences.getInt("editHomeFormation", 1);
            Log.d("HOME form", String.valueOf(form));
            String[] arrayItems = {"4-3-3","4-4-2","3-4-2"};
            String savedFormation = arrayItems[form];
            Log.d("HOME savedFormation", savedFormation);
            String[] formation = savedFormation.split("-");

            int defenders = Integer.parseInt(formation[0]);
            int midfielders = Integer.parseInt(formation[1]);
            int forwards = Integer.parseInt(formation[2]);


            List<CustomList> customList = new ArrayList<CustomList>();
            List<CustomList> keeper = new ArrayList<>();
            keeper.add(customList.get(0));

            List<CustomList> defender = new ArrayList<>();
            int i, j = 0;

            for (i = 1; i < defenders+1; i++) {
                defender.add(customList.get(i));
                j = i;
            }

            List<CustomList> midfield = new ArrayList<>();
            for (i = j+1; i < defenders + midfielders + 1; i++) {
                midfield.add(customList.get(i));
                j = i;
            }

            List<CustomList> forward = new ArrayList<>();
            for (i = j+1; i < defenders + midfielders + forwards + 1; i++) {
                forward.add(customList.get(i));
            }

            PitchAdapter keeperAdapter =
                    new PitchAdapter(getActivity(), keeper, null, Constants.HOME_TEAM, Constants.STARTING);
            keeperView.setAdapter(keeperAdapter);

            PitchAdapter defenderAdapter =
                    new PitchAdapter(getActivity(), defender, null, Constants.HOME_TEAM, Constants.STARTING);
            defenderView.setAdapter(defenderAdapter);

            PitchAdapter midfieldAdapter =
                    new PitchAdapter(getActivity(), midfield, null, Constants.HOME_TEAM, Constants.STARTING);
            midfieldView.setAdapter(midfieldAdapter);

            PitchAdapter forwardAdapter =
                    new PitchAdapter(getActivity(), forward, null, Constants.HOME_TEAM, Constants.STARTING);
            forwardView.setAdapter(forwardAdapter);

            PitchAdapter benchAdapter =
                    new PitchAdapter(getActivity(), customList, null, Constants.HOME_TEAM, Constants.BENCH);
            benchView.setAdapter(benchAdapter);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setEmptyList(boolean visibility) {
    //    textEmptyList.setVisibility(visibility ? View.VISIBLE : View.GONE);
    //    benchView.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

}