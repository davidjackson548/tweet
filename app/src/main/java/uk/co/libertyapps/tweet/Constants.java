package uk.co.libertyapps.tweet;

public class Constants {
    public static final int PRE_GAME = 0;
    public static final int FIRST_HALF = 1;
    public static final int SECOND_HALF = 2;
    public static final int HALF_TIME = 6;
    public static final int EXTRA_HALF_TIME = 7;
    public static final int HALF_LENGTH = 45;
    public static final int GAME_LENGTH = 90;
    public static final int HOME_TEAM = 1;
    public static final int AWAY_TEAM = 2;
    public static final int SIZE_OF_LINEUP = 11;
    public static final int SIZE_OF_SQUAD = 100;
    public static final int SUB_POS = -1;

    public static final int HOME_PAGER_ID = 0;
    public static final int EVENT_PAGER_ID = 1;
    public static final int AWAY_PAGER_ID = 2;
    public static final int COMMENTARY_PAGER_ID = 3;


    public static final String AUTHORITY = "uk.co.libertyapps.tweet.provider";

    public static final String TWITTER_REG_EX = "(?<=^|(?<=[^a-zA-Z0-9-_\\\\.]))@[A-Za-z0-9_-]+";

    public static final int EXTRA_TIME_FIRST = 3;
    public static final int EXTRA_TIME_SECOND = 4;
    public static final int PENALTIES = 5;
    public static final int FULL_TIME = 6;
    public static final int EXTRA_TIME_HALF_LENGTH = 15;

    public static final int STARTING = 1;
    public static final int BENCH = 2;

    public static final int CAT_ATTEMPTS = 1;
    public static final int CAT_SET_PIECES = 2;
    public static final int CAT_FOULS = 3;
    public static final int CAT_DEF = 4;
    public static final int CAT_MID = 5;
    public static final int CAT_ATTEMPTS_SHOT = 6;
    public static final int CAT_ATTEMPTS_HEAD = 7;
    public static final int CAT_ATTEMPTS_VOLLEY = 8;
    public static final int CAT_GK = 9;

    public static final  String[] EVENT_ITEMS = {"Events","shot OT","header OT","volley OT","shot wide","header wide","volley wide",
            "Shot Over","header over","volley over","header hits woodwork","shot hits woodwork","volley hits",
            "Goal",
            "Own Goal",
            "Miss Pen",
            "Convert Pen",
            "Hatrick Goal",
            "Handball",
            "Foul",
            "Red card",
            "Yellow card",
            "Full time",
            "Going to shootout",
            "Won shootout",
            "Creates chance",
            "Cross from left",
            "Cross from right",
            "Sub",
            "Appeal for pen not given",
            "Physio on",
            "Goal disallowed",
            "Half time",
            "Full time",
            "Penalty retake",
            "Ref warns player",
            "Player fight",
            "Keeper holds ball",
            "Player on rebound",
            "Keeper saves pen",
            "Keeper tips to safety",
            "Kick off",
            "Short corner",
            "Long corner",
            "Match abandoned",
            "Injury time count",
            "Free kick wall block",
            "Free kick",
            "Tackle",
            "Penalty awarded",
            "Kick off 2nd",
            "One on one",
            "Dribble down left",
            "Dribble down right",
            "Rounds keeper",
            "Injury no physio",
            "Throw in",
            "Goal kick",
            "Offside",
            "Block",
            "Slide tackle",
            "Defender header",
            "Defender clears"
    };

}
