package uk.co.libertyapps.tweet.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.Team;
import uk.co.libertyapps.tweet.fragment.CommentaryFragment;
import uk.co.libertyapps.tweet.fragment.FirstFragment;
import uk.co.libertyapps.tweet.fragment.PlayerFragment;


public class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.CustomListViewHolder> {
    Activity mContext;
    List<CustomList> mCustomList;
    Listener mListener;
    int team, selected;

    public SelectionAdapter(Activity context, List<CustomList> customList,
                            Listener listener, int team, int selected) {
        this.mCustomList = customList;
        this.mContext = context;
        this.mListener = listener;
        this.team = team;
        this.selected = selected;
    }

    @Override
    public CustomListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_select, parent, false);
        return new CustomListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomListViewHolder holder, int position) {
        CustomList customList = mCustomList.get(position);
        holder.textName.setText(customList.name);
        holder.textMobile.setText(customList.mobile);
        holder.setIsRecyclable(false);


        switch (mCustomList.get(position).status) {
            case 1:
                holder.radioStart.setBackgroundColor(Color.parseColor("#99edc3"));
                break;
            case 2:
                holder.radioBench.setBackgroundColor(Color.parseColor("#FFA500"));
                break;
            case 3:
                holder.radioNone.setBackgroundColor(Color.parseColor("#FFA07A"));
                break;
        }

        holder.radioNone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("onRadioButtonClicked", "radioNone");
                holder.radioStart.setBackgroundColor(0);
                holder.radioBench.setBackgroundColor(0);
                holder.radioNone.setBackgroundColor(Color.parseColor("#FFA07A"));
                mCustomList.get(position).status = 3;

                countSelected();
            }
        });

        holder.radioBench.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("onRadioButtonClicked", "radioBench");
                holder.radioStart.setBackgroundColor(0);
                holder.radioBench.setBackgroundColor(Color.parseColor("#FFA500"));
                holder.radioNone.setBackgroundColor(0);

                mCustomList.get(position).status = 2;

                countSelected();
            }
        });

        holder.radioStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("onRadioButtonClicked", "radioStart");
                holder.radioStart.setBackgroundColor(Color.parseColor("#99edc3"));
                holder.radioBench.setBackgroundColor(0);
                holder.radioNone.setBackgroundColor(0);

                mCustomList.get(position).status = 1;

                countSelected();
            }
        });
        holder.cardView.setTag(position);

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d("SA onLongClick", String.valueOf(position));
                Log.d("SA selected", String.valueOf(selected));

                FragmentManager fm = ((FragmentActivity) view.getContext()).getSupportFragmentManager();
                PlayerFragment pf = PlayerFragment.newInstance();
                pf.setArguments(mCustomList, team, position, selected);
                pf.setShowsDialog(true);

                pf.show(fm, "PlayerFragment");

                     /* ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction().replace(R.id.eventFrag,
                              pf).commit();*/

                return true;
            }
        });
    }

    private void countSelected() {
        // count
        int start = 0;
        Pattern pattern = Pattern.compile("status=1", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(mCustomList.toString());
        while (matcher.find()) {
            start++;
        }

        int sub = 0;
        pattern = Pattern.compile("status=2", Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(mCustomList.toString());
        while (matcher.find()) {
            sub++;
        }
        Toast.makeText(mContext, start + " starts / " + sub + " benched" , Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return mCustomList.size();
    }

    public List<CustomList> getCustomList() {
        return mCustomList;
    }

    public void updateCustomList(List<CustomList> customList) {
        this.mCustomList = customList;
    }

    public interface Listener {
        void setEmptyList(boolean visibility);
    }

    public class CustomListViewHolder extends RecyclerView.ViewHolder {
        TextView textName;
        TextView textMobile;
        LinearLayout cardView;
        RadioButton radioStart, radioBench, radioNone;

        public CustomListViewHolder(View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.textName);
            textMobile = itemView.findViewById(R.id.textMobile);
            cardView = itemView.findViewById(R.id.cardView);
            radioStart = itemView.findViewById(R.id.radioStart);
            radioBench = itemView.findViewById(R.id.radioBench);
            radioNone = itemView.findViewById(R.id.radioNone);
        }

    }
}
