package uk.co.libertyapps.tweet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.core.content.FileProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class ImageCreation<bounds> {

    public static void setLogo(Intent data, Activity a, int id, String png, String pref, String prefSave) {
        Uri uri = data.getData();
        Log.d("OAR setLogo", uri.toString());

        Bitmap bitmap;

        try {
            bitmap = MediaStore.Images.Media.getBitmap(a.getContentResolver(), uri);
            Log.d("OAR TAG", String.valueOf(bitmap));

            ImageView imageView = a.findViewById(id);
            imageView.setImageBitmap(bitmap);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 40, bytes);
            String destPath = a.getExternalFilesDir(null).getAbsolutePath() + png;
            try {
                OutputStream out = new FileOutputStream(destPath);
                try {
                    out.write(bytes.toByteArray());
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Log.d("destPath", destPath);

            //todo save the file
            SharedPreferences sharedpreferences = a.getSharedPreferences(pref, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(prefSave, destPath);
            editor.commit();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createImageFromBitmap(Context context, Bitmap bit, String file, String message) {
        Bitmap drawable = bit;
        int w = drawable.getWidth(), h = drawable.getHeight();
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap(w, h, conf); // this creates a MUTABLE bitmap
        Canvas canvas = new Canvas(bmp);
        canvas.drawBitmap(drawable, 0, 0, null);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        String destPath = context.getExternalFilesDir(null).getAbsolutePath() + file;
        try {
            OutputStream out = new FileOutputStream(destPath);
            try {
                out.write(bytes.toByteArray());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("destPath", destPath);

        Uri backgroundAssetUri = FileProvider.getUriForFile(context, "uk.co.libertyapps.tweet.provider", new File(destPath));

        Intent intent = new Intent();
        intent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");//Set MIME Type
        intent.putExtra(Intent.EXTRA_TEXT, message);
        Uri screenshotUri = backgroundAssetUri;
        intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);// Pur Image to intent

        //       Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        //       tweetIntent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
//        tweetIntent.setType("text/plain");
//        tweetIntent.putExtra(Intent.EXTRA_TEXT, MainActivity.generateTemplate(MainActivity.tweet));

        PackageManager packManager = context.getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent,  PackageManager.MATCH_DEFAULT_ONLY);
        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList){
            if(resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")){
                intent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if (resolved) {
            // launch in app
            context.startActivity(intent);
        } else {
            // launch in browser
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + message;
            Log.d("tweetUrl", tweetUrl);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(tweetUrl));
            context.startActivity(i);
        }
    }

    public static void createImageFromDrawable(Context context, int id, String file, String message) {
        Bitmap drawable = BitmapFactory.decodeResource(context.getResources(), id);
        int w = drawable.getWidth(), h = drawable.getHeight();
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap(w, h, conf); // this creates a MUTABLE bitmap
        Canvas canvas = new Canvas(bmp);
        canvas.drawBitmap(drawable, 0, 0, null);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        String destPath = context.getExternalFilesDir(null).getAbsolutePath() + file;
        try {
            OutputStream out = new FileOutputStream(destPath);
            try {
                out.write(bytes.toByteArray());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("destPath", destPath);

        Uri backgroundAssetUri = FileProvider.getUriForFile(context, "uk.co.libertyapps.tweet.provider", new File(destPath));

        Intent intent = new Intent();
        intent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");//Set MIME Type
        intent.putExtra(Intent.EXTRA_TEXT, message);
        Uri screenshotUri = backgroundAssetUri;
        intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);// Pur Image to intent

 //       Intent tweetIntent = new Intent(Intent.ACTION_SEND);
 //       tweetIntent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
//        tweetIntent.setType("text/plain");
//        tweetIntent.putExtra(Intent.EXTRA_TEXT, MainActivity.generateTemplate(MainActivity.tweet));

        PackageManager packManager = context.getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent,  PackageManager.MATCH_DEFAULT_ONLY);
        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList){
            if(resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")){
                intent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if (resolved) {
            // launch in app
            context.startActivity(intent);
        } else {
            // launch in browser
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + message;
            Log.d("tweetUrl", tweetUrl);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(tweetUrl));
            context.startActivity(i);
        }
    }

    public static void createScoreImage(Context context, int half, String message) {
        Bitmap bmp2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.fylde);
        Bitmap bmp3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.fylde);

        int drawable = 0;
        if (half == 1 ) {
            drawable = R.drawable.halftime;
        }
        if (half == 2 ) {
            drawable = R.drawable.full_time;
        }
        // Log.d("createScoreImage", "createScoreImage");

        SharedPreferences home = context.getSharedPreferences("home", Context.MODE_PRIVATE);
        String updated_home = home.getString("editHomeLogo", "");
        if (!updated_home.equals("")) {
            Uri uri_h = Uri.parse(updated_home);
            bmp2 = BitmapFactory.decodeFile(String.valueOf(uri_h));
        }

        SharedPreferences away = context.getSharedPreferences("away", Context.MODE_PRIVATE);
        String updated_away = away.getString("editAwayLogo", "");
        if (!updated_away.equals("")) {
            Uri uri_a = Uri.parse(updated_away);
            bmp3 = BitmapFactory.decodeFile(String.valueOf(uri_a));
        }

        Bitmap bmp1 = BitmapFactory.decodeResource(context.getResources(), drawable);

        Bitmap bmp4 = Bitmap.createScaledBitmap(bmp2, 150, 150, true);
        Bitmap bmp5 = Bitmap.createScaledBitmap(bmp3, 150, 150, true);
        Bitmap bmp6 = Bitmap.createScaledBitmap(bmp1, 1024, 512, true);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        int w = 1024, h = 512;
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap(w, h, conf); // this creates a MUTABLE bitmap

        Canvas canvas = new Canvas(bmp);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(context.getResources().getColor(R.color.fylde));
        paint.setTextSize(120);
        paint.setFakeBoldText(true);

        canvas.drawBitmap(bmp6, 0, 0, paint);
        canvas.drawBitmap(bmp4, 352, 30, null);
        canvas.drawBitmap(bmp5, 562, 30, null);
        canvas.drawText(String.valueOf(MainActivity.homeGoal), 412, 420, paint);
        canvas.drawText("-", 500, 420, paint);
        canvas.drawText(String.valueOf(MainActivity.awayGoal), 562, 420, paint);

        bmp1.recycle();
        bmp2.recycle();
        bmp3.recycle();
        bmp4.recycle();
        bmp5.recycle();
        bmp6.recycle();

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        String destPath =  context.getExternalFilesDir(null).getAbsolutePath() + "/new.jpg";
        try {
            OutputStream out = new FileOutputStream(destPath);
            try {
                out.write(bytes.toByteArray());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Log.d("destPath", destPath);

        Uri backgroundAssetUri = FileProvider.getUriForFile(context, "uk.co.libertyapps.tweet.provider", new File(destPath));

        Intent intent= new Intent();
        intent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");//Set MIME Type
        intent.putExtra(Intent.EXTRA_TEXT, MainActivity.generateTemplate(message));
        Uri screenshotUri = backgroundAssetUri;
        intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);// Pur Image to intent

        PackageManager packManager = context.getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent,  PackageManager.MATCH_DEFAULT_ONLY);
        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList){
            if(resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")){
                intent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if (resolved) {
            // launch in app
            context.startActivity(intent);
        } else {
            // launch in browser
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + message;
            Log.d("tweetUrl", tweetUrl);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(tweetUrl));
            context.startActivity(i);
        }


        //todo insta


/*         Intent storiesIntent = new Intent("com.instagram.share.ADD_TO_STORY");
        storiesIntent.setDataAndType(backgroundAssetUri,  "image/*");
                    storiesIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    storiesIntent.setPackage("com.instagram.android");
                    context.grantUriPermission(
                            "com.instagram.android", backgroundAssetUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(storiesIntent);*/

   /*     Intent feedIntent = new Intent(Intent.ACTION_SEND);
        feedIntent.setDataAndType(backgroundAssetUri,  "image/*");
        feedIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        feedIntent.setPackage("com.instagram.android");
        context.grantUriPermission(
                "com.instagram.android", backgroundAssetUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

        feedIntent.putExtra(Intent.EXTRA_STREAM, backgroundAssetUri);
        context.startActivity(feedIntent);*/
    }

    public static void createSubImage(Context context, String tweet, int in, int off) {

        int onId = context.getResources().getIdentifier("p" + in, "drawable", context.getPackageName());
        int offId = context.getResources().getIdentifier("p" + off, "drawable", context.getPackageName());



        Bitmap bmp2 = BitmapFactory.decodeResource(context.getResources(), onId);
        Bitmap bmp3 = BitmapFactory.decodeResource(context.getResources(), offId);

        Bitmap bmp4 = Bitmap.createScaledBitmap(bmp2, 342, 512, true);
        Bitmap bmp5 = Bitmap.createScaledBitmap(bmp3, 342, 512, true);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        int w = 682, h = 512;
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap(w, h, conf); // this creates a MUTABLE bitmap

        Canvas canvas = new Canvas(bmp);


        canvas.drawBitmap(bmp4, 0, 0, null);
        canvas.drawBitmap(bmp5, 342, 100, null);

        Paint red = new Paint(Paint.ANTI_ALIAS_FLAG);
        red.setColor(context.getResources().getColor(R.color.red));
        red.setTextSize(80);
        red.setFakeBoldText(true);

        Paint green = new Paint(Paint.ANTI_ALIAS_FLAG);
        green.setColor(context.getResources().getColor(R.color.green));
        green.setTextSize(80);
        green.setFakeBoldText(true);

        canvas.drawText("ON", 10, 420, green);
        canvas.drawText("OFF", 542, 420, red);

        bmp2.recycle();
        bmp3.recycle();
        bmp4.recycle();
        bmp5.recycle();


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        String destPath =  context.getExternalFilesDir(null).getAbsolutePath() + "/new.jpg";
        try {
            OutputStream out = new FileOutputStream(destPath);
            try {
                out.write(bytes.toByteArray());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Log.d("destPath", destPath);

        Uri backgroundAssetUri = FileProvider.getUriForFile(context, "uk.co.libertyapps.tweet.provider", new File(destPath));

        Intent intent= new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");//Set MIME Type
        intent.putExtra(Intent.EXTRA_TEXT, MainActivity.generateTemplate(tweet));
        Uri screenshotUri = backgroundAssetUri;
        intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);// Pur Image to intent

        intent.setPackage("com.twitter.android");
        context.startActivity(intent);
    }

    public static void drawTeamsheet(Context context, List<CustomList> str, String post) {
        if (str.size() > 10) {
            CustomList.order(str);


            int w = 4000, h = 4000;

            //todo how to load lineup image
            Bitmap lineup = BitmapFactory.decodeResource(context.getResources(), R.drawable.lineup);
            Bitmap mutableBitmap = lineup.copy(Bitmap.Config.ARGB_8888, true);


            Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
            Bitmap bmp = Bitmap.createBitmap(w, h, conf); // this creates a MUTABLE bitmap

            Canvas canvas = new Canvas(bmp);
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.ADD);


            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(context.getResources().getColor(R.color.fylde));
            paint.setTextSize(100);

            canvas.drawBitmap(mutableBitmap, 0, 0, paint);

            Rect bounds = new Rect();

            int yoff = 0;
            int fix = 180;
            for (int i = 0; i < 5; ++i) {
                String player = str.get(i).mobile + " " + str.get(i).name;
                canvas.drawText(player.toUpperCase(), 50, 2300 + yoff, paint);
                paint.getTextBounds(player.toUpperCase(), 0, player.length(), bounds);
                yoff += bounds.height() + fix;
                //    yoff += fix;
            }

            yoff = 0;
            bounds = new Rect();
            for (int i = 5; i < 10; ++i) {
                String player = str.get(i).mobile + " " + str.get(i).name;
                canvas.drawText(player.toUpperCase(), 1300, 2300 + yoff, paint);
                paint.getTextBounds(player.toUpperCase(), 0, player.length(), bounds);
                yoff += bounds.height() + fix;
                //    yoff += fix;
            }

            yoff = 0;
            bounds = new Rect();
            for (int i = 10; i < 11; ++i) {
                String player = str.get(i).mobile + " " + str.get(i).name;
                canvas.drawText(player.toUpperCase(), 2550, 2300 + yoff, paint);
                paint.getTextBounds(player.toUpperCase(), 0, player.length(), bounds);
                yoff += bounds.height() + fix;
                //    yoff += fix;
            }

            yoff = 0;
            bounds = new Rect();
            String subs = "SUBS";
            canvas.drawText(subs, 2550, 2600 + yoff, paint);
            paint.getTextBounds(subs, 0, subs.length(), bounds);

            yoff = 0;
            bounds = new Rect();
            for (int i = 11; i < 16; ++i) {
                String player = str.get(i).mobile + " " + str.get(i).name;
                canvas.drawText(player.toUpperCase(), 2550, 2800 + yoff, paint);
                paint.getTextBounds(player.toUpperCase(), 0, player.length(), bounds);
                yoff += bounds.height() + 100;
                //    yoff += fix;
            }

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

            String destPath = context.getExternalFilesDir(null).getAbsolutePath() + "/new.gif";

            try {
                OutputStream out = new FileOutputStream(destPath);
                try {
                    out.write(bytes.toByteArray());
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Log.d("destPath", destPath);

            Uri backgroundAssetUri = FileProvider.getUriForFile(context, Constants.AUTHORITY, new File(destPath));

            Intent intent = new Intent();
            intent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("image/*");//Set MIME Type
            intent.putExtra(Intent.EXTRA_TEXT, post);
            intent.putExtra(Intent.EXTRA_STREAM, backgroundAssetUri);// Pur Image to intent

            intent.setPackage("com.twitter.android");
            context.startActivity(intent);
        }
    }
}