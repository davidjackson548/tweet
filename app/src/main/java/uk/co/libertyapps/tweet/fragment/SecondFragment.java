package uk.co.libertyapps.tweet.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.ImageCreation;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.OptionActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.Team;
import uk.co.libertyapps.tweet.adapters.SelectionAdapter;

public class SecondFragment extends Fragment {
    TextView textEmptyList;
    int SELECT_OCR_PICTURE = 200;
    static EditText editTeamName, editEmoji;
    public static EditText editTwitter;
    public static EditText editLineUp;
    public static ImageView teamLogo;
    public static SelectionAdapter mCustomListAdapter;
    public static List<CustomList> teamList = new ArrayList<CustomList>();
    RecyclerView recyclerView;
    TextView textSquadCount;
    int tab = 99;
    final String lineup = "editAwayLineUp", teamname = "editAwayTeamName", emoji = "editAwayEmoji", twitter =  "editAwayTwitter";

    public SecondFragment() {
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("away", Context.MODE_PRIVATE);
        editTeamName = view.findViewById(R.id.editTeamName);
        editEmoji = view.findViewById(R.id.editEmoji);
        editLineUp = view.findViewById(R.id.editLineUp);
        editTwitter = view.findViewById(R.id.editTwitter);
        teamLogo = view.findViewById(R.id.teamLogo);
        teamLogo.setId(R.id.awayLogo);
        textEmptyList = view.findViewById(R.id.textEmptyList);
        recyclerView = view.findViewById(R.id.recyclerView);
        textSquadCount = view.findViewById(R.id.textSquadCount);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        editTeamName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                // Log.d("afterTextChanged", s.toString());
                String fa = "fulltime.thefa.com";
                if (s.toString().contains(fa)) {
                    //        Team.gatImport(getContext(), Constants.HOME_TEAM, FirstFragment.editLineUp);
                    Team.Content grab = new Team.Content();
                    grab.url = s.toString();
                    grab.mCustomListAdapter = mCustomListAdapter;
                    grab.teamList = teamList;
                    grab.editTeamName = editTeamName;
                    grab.execute();
                    // editTeamName.setText("Fylde");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        editLineUp.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.toString().contains(System.lineSeparator())) {
                    // add row
                    List<CustomList> customList;
                    //   CustomList row = new CustomList();
                    String[] squad = new String[100];
                    if (s.length() != 0) {
                        String input = s.toString();
                        String lineSep = ","; // System.getProperty("line.separator");
                        input = input.toString().replace(System.getProperty("line.separator"), lineSep);
                        input = input.replace("/", lineSep);
                        squad = input.split(lineSep);
                        String name, mobile;
                        customList = new ArrayList<CustomList>();
                        // get the names and numbers
                        for (int i = 0; i < squad.length; i++) {

                            CustomList row = new CustomList();
                            row.mobile = squad[i].replaceAll("([^0-9])", "");

                            if (row.mobile.equals("")) {
                                row.mobile = "0";
                            }
                            if (Integer.parseInt(row.mobile) > 99) {
                                row.mobile = "0";
                            }

                            row.name = squad[i].replaceAll("[\\d\\\\-\\\\+\\\\.\\\\^:,]", "");
                            row.status = 3;
                            customList.add(i, row);
                            teamList.add(row);
                            mCustomListAdapter.notifyDataSetChanged();
                        }
                    }
                    editLineUp.setText("");
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        String OCR = sharedpreferences.getString("editAwayOCR", "");

        teamList = Team.loadTeam(getActivity(), Constants.AWAY_TEAM);
        CustomList.orderForSelection(teamList);

        editTeamName.setText(sharedpreferences.getString("editAwayTeamName", ""));
        editEmoji.setText(sharedpreferences.getString("editAwayEmoji", ""));
        editLineUp.setText(WordUtils.capitalizeFully("" + OCR));
        editTwitter.setText(sharedpreferences.getString(twitter, ""));

        Uri uri = Uri.parse(sharedpreferences.getString("editAwayLogo", ""));
        if (uri.toString().length() > 0) {
            Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(uri));
            //    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            teamLogo.setImageBitmap(bitmap);

        }

        teamLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Open", "GALLERY");
                //todo how to open gallery?
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Away Logo"), OptionActivity.SELECT_AWAY);
            }
        });

        view.findViewById(R.id.ocrTeam).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("SF", "ocrTeam");
                OptionActivity.team = tab;
                Log.d("SF", "team ocrTeam = " + OptionActivity.team);
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_OCR_PICTURE);

            }
        });

        view.findViewById(R.id.saveTeam).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Team.compareSquads(getContext(), Constants.AWAY_TEAM, teamList, editTeamName.getText().toString());
            }
        });

        try {
            if (teamList.size() > 0) {
                textEmptyList.setVisibility(View.GONE);
            }
            mCustomListAdapter =
                    new SelectionAdapter(getActivity(), teamList, null, Constants.AWAY_TEAM, 0);
            recyclerView.setAdapter(mCustomListAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("SF", "onSaveInstanceState");
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("away", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        String lineup = "", teamname = "", emoji = "";

        lineup = "editAwayLineUp";
        teamname = "editAwayTeamName";
        emoji = "editAwayEmoji";

        if (editLineUp.getText().toString() != null) {
            String s = editLineUp.getText().toString();
            // Log.d("editLineUp", s);
            String[] squad = new String[100];
            if (s.length() != 0) {

                String lineSep = ","; // System.getProperty("line.separator");
                s = s.replace(System.getProperty("line.separator"), lineSep);
                s = s.replace("/", lineSep);
                squad = s.split(lineSep);
                String name, mobile;
                List<CustomList> customList = new ArrayList<CustomList>();
                // get the names and numbers
                for (int i = 0; i < squad.length; i++) {

                    // Log.d("trim", squad[i].trim());
                    CustomList row = new CustomList();
                    row.mobile = squad[i].replaceAll("([^0-9])", "");

                    if (row.mobile.equals("")) {
                        row.mobile = "0";
                    }
                    if (Integer.parseInt(row.mobile) > 99) {
                        row.mobile = "0";
                    }

                    row.name = squad[i].replaceAll("[\\d\\\\-\\\\+\\\\.\\\\^:,]", "");
                    row.status = 3;
                    customList.add(i, row);
                }

                editor.putString("editAwayList", customList.toString() + teamList.toString());
            } else {
                editor.putString("editAwayList", teamList.toString());

            }
            editor.putString("editAwayOCR", "");

            editor.putString(lineup, editLineUp.getText().toString());
        }

        if (editTeamName.getText().toString() != null) {
            editor.putString(teamname, WordUtils.capitalizeFully(editTeamName.getText().toString()));
        }

        if (editTwitter.getText().toString() != null) {
            editor.putString(twitter, editTwitter.getText().toString());
        }

        if (editEmoji.getText().toString() != null) {
            editor.putString(emoji, editEmoji.getText().toString());
        }
        editor.apply();
    }

}