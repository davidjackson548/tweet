package uk.co.libertyapps.tweet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import uk.co.libertyapps.tweet.adapters.OptionPageAdapter;
import uk.co.libertyapps.tweet.fragment.ThirdFragment;

public class OptionActivity extends AppCompatActivity {

    public static final int SELECT_OCR_IMAGE = 200, SELECT_HOME = 101, SELECT_AWAY = 102, SELECT_FULL_TIME = 103, SELECT_HALF_TIME = 104,
            SELECT_GOAL_HOME = 105, SELECT_GOAL_AWAY = 106, SELECT_LINEUP = 107, SELECT_ZIP = 987;
    public static int team = 0;
    static ViewPager2 viewPager;
    static OptionPageAdapter myAdapter;
    static TabLayout tabLayout;
    String url = "";
    String title = "", body = "";
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        setContentView(R.layout.activity_options);

        myAdapter = new OptionPageAdapter(getSupportFragmentManager(), getLifecycle());
        viewPager = findViewById(R.id.oPager);
        viewPager.setAdapter(myAdapter);
//        viewPager.setCurrentItem(2);
//        viewPager.setOffscreenPageLimit(3); // fix for LIV-55 crash

        tabLayout = findViewById(R.id.tab_layout);
        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> tab.setText("TEAM " + (position + 1))
        ).attach();
        tabLayout.getTabAt(0).setText("Home");
        tabLayout.getTabAt(1).setText("Away");
        tabLayout.getTabAt(2).setText("Game");

        if (getIntent() != null) {
            if (getIntent().hasExtra("Team")) {
                team = getIntent().getIntExtra("Team", 0);
                Log.d("getteam", String.valueOf(team));
                viewPager.setCurrentItem(team - 1);
            }

            // this is OCR code
            if (getIntent().getStringExtra("row") != null) {
                String row = getIntent().getStringExtra("row");
                Log.d("getIntent", row);
                SharedPreferences.Editor editor;
                SharedPreferences sharedpreferences;

                if (team == 1) {
                    sharedpreferences = getSharedPreferences("home", Context.MODE_PRIVATE);
                    editor = sharedpreferences.edit();
                    editor.putString("editHomeOCR", row);
                    editor.commit();
                    viewPager.setCurrentItem(team - 1);
                }
                if (team == 2) {
                    sharedpreferences = getSharedPreferences("away", Context.MODE_PRIVATE);
                    editor = sharedpreferences.edit();
                    editor.putString("editAwayOCR", row);
                    editor.commit();
                    viewPager.setCurrentItem(team - 1);
                }
            }
        }

    }

    public static File getFile(Context context, Uri uri) throws IOException {
        File destinationFilename = new File(context.getFilesDir().getPath() + File.separatorChar + "Zipfile.zip");
        try (InputStream ins = context.getContentResolver().openInputStream(uri)) {
            createFileFromStream(ins, destinationFilename);
        } catch (Exception ex) {
            Log.e("Save File", ex.getMessage());
            ex.printStackTrace();
        }
        return destinationFilename;
    }

    public static void createFileFromStream(InputStream ins, File destination) {
        try (OutputStream os = new FileOutputStream(destination)) {
            byte[] buffer = new byte[4096];
            int length;
            while ((length = ins.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            os.flush();
        } catch (Exception ex) {
            Log.e("Save File", ex.getMessage());
            ex.printStackTrace();
        }
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("onActivityResult", "onActivityResult");
        Log.d("requestCode", String.valueOf(requestCode));
        Log.d("resultCode", String.valueOf(resultCode));

        if (resultCode == RESULT_OK) {
            Log.d("onActivityResult", "RESULT_OK");
            /*if (requestCode > 0 && requestCode < 50) {
                // todo how know the player?
                Log.d("RESULT_OK requestCode", String.valueOf(requestCode));
                team = Constants.HOME_TEAM;
                int player = requestCode;
                List<CustomList> mCustomList = Team.loadTeam(this, Constants.HOME_TEAM);
                ImageCreation.setLogo(data, this, R.id.goalImage, "mCustomList.get(player).id", "home", "x");
            }

            if (requestCode == SELECT_ZIP) {
                String destPath = getExternalFilesDir(null).getAbsolutePath() + "/";
                Log.d("destPath", destPath);
                Uri uri = data.getData();
                try {
                    File f = getFile(this, uri);
                    DecompressFast df= new DecompressFast(f.getPath(), destPath, this);
                    df.unzip();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/

            if (requestCode == SELECT_LINEUP) {
                Log.d("onActivityResult", "SELECT_LINEUP");
                ImageCreation.setLogo(data, this, R.id.startingLineup,"/lineup.png", "match", "editLineup");
            }

            if (requestCode == SELECT_HOME) {
                Log.d("onActivityResult", "SELECT_HOME");
                ImageCreation.setLogo(data, this, R.id.teamLogo,"/home.png", "home", "editHomeLogo");
            }

            if (requestCode == SELECT_AWAY) {
                Log.d("onActivityResult", "SELECT_AWAY");
                ImageCreation.setLogo(data, this, R.id.awayLogo,"/away.png", "away", "editAwayLogo");
            }

            if (requestCode == SELECT_FULL_TIME) {
                Log.d("onActivityResult", "SELECT_FULL_TIME");
                ImageCreation.setLogo(data, this, R.id.fullTime,"/fulltime.png", "match", "editFullTime");
            }

            if (requestCode == SELECT_HALF_TIME) {
                Log.d("onActivityResult", "SELECT_HALF_TIME");
                ImageCreation.setLogo(data, this, R.id.halfTime,"/halftime.png", "match", "editHalfTime");
            }

            if (requestCode == SELECT_GOAL_HOME) {
                Log.d("onActivityResult", "SELECT_GOAL_HOME");
                ImageCreation.setLogo(data, this, R.id.teamGoal,"/goalhome.png", "match", "editHomeGoal");
            }

/*            if (requestCode == SELECT_GOAL_AWAY) {
                Log.d("onActivityResult", "SELECT_GOAL_HOME");
                ImageCreation.setLogo(data, this, R.id.teamGoal,"/goalaway.png", "home", "editAwayGoal");
            }*/

            if (requestCode == SELECT_OCR_IMAGE) {

                // Get the url of the image from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {

                    // update the preview image in the layout
                    Log.d("selectedImageUri", selectedImageUri.toString());

                    Intent shareIntent = new Intent(OptionActivity.this, OCRActivity.class);
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, selectedImageUri);
                    shareIntent.setType("image/jpeg");

                    // get team on ocr activity and pass back?
                    Log.d("`onActivityResult", "TEAM" + String.valueOf(team));
                    shareIntent.putExtra("Team", team);

                    startActivityForResult(shareIntent, 123);
                }
            }
        }
    }

}