package uk.co.libertyapps.tweet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.util.CollectionUtils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import uk.co.libertyapps.tweet.adapters.SelectionAdapter;
import uk.co.libertyapps.tweet.fragment.FirstFragment;
import uk.co.libertyapps.tweet.fragment.SecondFragment;
import uk.co.libertyapps.tweet.fragment.ThirdFragment;

public class Team {

    public static String gatSaved(Context context, int team) {
        String key = "", pref = "";
        if (team == Constants.HOME_TEAM) {
            key = "editHomeLineUp";
            pref = "home";
        }
        if (team == Constants.AWAY_TEAM) {
            key = "editAwayLineUp";
            pref = "away";
        }
        SharedPreferences sharedpreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE);
        String squadList = sharedpreferences.getString(key, "");
        // Log.e("gatSaved squadList", "" + squadList);
        return squadList;
    }

    //todo not sure why crash
    public static List<CustomList> loadTeam(Context mContext, int team ) {
        String key = "", pref = "";
        if (team == Constants.HOME_TEAM) {
            key = "editHomeList";
            pref = "home";
        }
        if (team == Constants.AWAY_TEAM) {
            key = "editAwayList";
            pref = "away";
        }
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(pref, Context.MODE_PRIVATE);
        String squadList = sharedpreferences.getString(key, "");
        // Log.e("loadTeam squadList", "" + squadList);

        List<CustomList> customList = new ArrayList<CustomList>();

        if (squadList.length() > 0) {
            String lineSep = "\\[";
            squadList = squadList.replace(System.getProperty("line.separator"), lineSep);
            squadList = squadList.replace("/", lineSep);
            String[] squad  = squadList.split(lineSep);
            // Log.e("loadTeam squad[]", "" + squad.length);
            int playLimit = Math.min(99, squad.length);
            int k = 0;
            String mobile = "", name = "", status = "";
            for (int i = 1; i < playLimit; i++) {
                squad[i] = squad[i].replace("id=<null>,mobile=", "");
                squad[i] = squad[i].replace("name=", "");
                squad[i] = squad[i].replace("twitter=", "");
           //     Log.e("loadTeam squad[i]", "" + squad[i]);
                String[] player  = squad[i].split(",");
            //     Log.e("loadTeam player", "" + player.length);
                for (int j = 0; j < player.length; j++) {
                   //  Log.e("loadTeam player" + j, "" + player[j]);
                   if (j == 0) {
                        mobile = player[0];
                    }
                    if (j == 1) {
                        name = player[1];
                    }
                    if (j == 2) {
                        status = player[2].replace("status=", "");;

                    //  Log.e("loadTeam player = ", "" + mobile + name);
                        CustomList row = new CustomList();
                        row.mobile = mobile;
                        row.name = name.trim();
                        row.status = Integer.parseInt(status);
                         customList.add(k, row);
                         k++;
                    }
                }
            }
        }
        return customList;
    }

    public static void saveTeam(Context mContext, int team ) {
        String key = "", save = "", list = "";
        if (team == Constants.HOME_TEAM) {
            MainActivity.homeTeamList = new ArrayList<CustomList>();
 //           MainActivity.homeTeamList.addAll(MainActivity.homeStartingList);
 //           MainActivity.homeTeamList.addAll(MainActivity.homeBenchList);
            key = "editHomeList";
            save = "home";
            list = MainActivity.homeTeamList.toString();
        }
        if (team == Constants.AWAY_TEAM) {
            MainActivity.awayTeamList = new ArrayList<CustomList>();
 //           MainActivity.awayTeamList.addAll(MainActivity.awayStartingList);
 //           MainActivity.awayTeamList.addAll(MainActivity.awayBenchList);
            key = "editAwayList";
            save = "away";
            list = MainActivity.awayTeamList.toString();
        }
       // Log.e("saveTeam list", list);

        SharedPreferences sharedpreferences = mContext.getSharedPreferences(save, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, list);
        editor.apply();
    }

    public static class SORT_BY_ID implements Comparator<CustomList> {
        public int compare(CustomList a, CustomList b)
        {
            return a.getNumber() - b.getNumber();
        }
    }

    public static void compareSquads(Context context, int team,  List<CustomList> teamList, String name) {
        // original
        List<CustomList> OriginSquad = Team.loadTeam(context, team);
        List<CustomList> orig = new ArrayList<CustomList>();
        int omin = Math.min(OriginSquad.size(), Constants.SIZE_OF_LINEUP);
        if (omin > 0) {
            orig.addAll(OriginSquad.subList(0, omin));
            Log.d("GET SQ", String.valueOf(orig.size()));
        }

        String start = ""; // string for lineup tweet
        String sub = ""; // string for sub tweet
        List<CustomList> starting = new ArrayList<CustomList>();
        List<CustomList> bench = new ArrayList<CustomList>();
        CustomList.order(teamList);
        int min = Math.min(teamList.size(), Constants.SIZE_OF_LINEUP);
        starting.addAll(teamList.subList(0, min));
        if (teamList.size() > Constants.SIZE_OF_LINEUP) {
            bench.addAll(teamList.subList(Constants.SIZE_OF_LINEUP, teamList.size()));

            //todo bench remove all not status = 2
            for (int i = 0; i < bench.size(); i++) {
                Log.d("bench", bench.get(i).toString());

                if (bench.get(i).status != 2) {
                    Log.d("remove", "remove");
                    bench.remove(i);
                    i--;
                }
            }
        }

        //  int j = 0; // used for bench
        for (int i = 0; i < teamList.size(); i++) {
            if (i < Constants.SIZE_OF_LINEUP) {
                start += starting.get(i).mobile.replaceAll("^0+", "") + " " + WordUtils.capitalizeFully(starting.get(i).name) + "\n";
            }
        }

        for (int j = 0; j < bench.size(); j++) {
            sub += bench.get(j).mobile.replaceAll("^0+", "") + " " + WordUtils.capitalizeFully(bench.get(j).name) + "\n";
            //   j++;
        }

        // compare changes here?
        int match = 0;
        int changes = 0;
        if (starting.size() > 0) {
            int smin = Math.min(starting.size(), Constants.SIZE_OF_LINEUP);

            for (int o = 0; o < omin; o++) {
                for (int s = 0; s < smin; s++) {
                    if (starting.get(s).name.equals(orig.get(o).name)) {
                        match++;
                        Log.d("Match", starting.get(s).name + " " + match);
                    }
                }
            }
            changes = starting.size() - match;
        }

        final String oppTwitter = getOppTwitter(team);
        Intent options = new Intent(context, MainActivity.class);
        context.startActivity(options);

        // send reserves to twitter
        Intent reserveIntent = new Intent(Intent.ACTION_SEND);
        reserveIntent.setClassName("com.twitter.android", "com.twitter.composer.ComposerActivity");
        reserveIntent.putExtra(Intent.EXTRA_TEXT, bench.size() + " subs on the bench today for " + name + "\n" + sub);
        reserveIntent.setType("text/plain");
        PackageManager packManager = context.getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(reserveIntent, PackageManager.MATCH_DEFAULT_ONLY);
        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                reserveIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved) {
            // launch in app
            context.startActivity(reserveIntent);
        } else {
            // launch in browser
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + bench.size() + " subs on the bench today for " + name + "\n" + sub;
            Log.d("tweetUrl", tweetUrl);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(tweetUrl));
            context.startActivity(i);
        }


        // context.startActivity(reserveIntent);
        String post = changes + " changes today for " + name + " vs " + oppTwitter + "\n" + start;
        Bitmap bitmap1 = null;

        SharedPreferences sharedpreferences = context.getSharedPreferences("match", Context.MODE_PRIVATE);
        Uri uri = Uri.parse(sharedpreferences.getString("editLineup", ""));
        Log.d("uri", uri.toString());
        if (uri.toString().length() > 0) {
            bitmap1 = BitmapFactory.decodeFile(String.valueOf(uri));
            ImageCreation.createImageFromBitmap(context, bitmap1, "/lineup.jpg", changes + " changes today for " + name + " vs " + oppTwitter + "\n" + start);
        } else {
            ImageCreation.createImageFromDrawable(context, R.drawable.lineless, "/lineup.jpg", changes + " changes today for " + name + " vs " + oppTwitter + "\n" + start);
        }
    }

    private static String getOppTwitter(int team) {
       /* if (team == Constants.HOME_TEAM) {
            if (SecondFragment.editTwitter != null)
            return SecondFragment.editTwitter.getText().toString();
        }
        if (team == Constants.AWAY_TEAM) {
            if (FirstFragment.editTwitter != null)
            return FirstFragment.editTwitter.getText().toString();
        }*/
        return  "";
    }

    public static String fixCom(String script) {
        if (MainActivity.sex) {
            script = script .replaceAll("\\bhe\'s\\b", "she's")
                    .replaceAll("\\bhe\'ll\\b", "she'll")
                    .replaceAll("\\bHe\'ll\\b", "She'll")
                    .replaceAll("\\bhimself\\b", "herself")
                    .replaceAll("\\bman\\b", "woman")
                    .replaceAll("\\bhis\\b", "her")
                    .replaceAll("\\bHe\\b", "She")
                    .replaceAll("\\bHis\\b", "Her")
                    .replaceAll("\\bhim\\b", "her")
                    .replaceAll("\\bhe\\b", "her");
        }

        return script.replace("<p1>", MainActivity.activePlayer)
                .replace("<Rf>", "Ref")
                .replace("<rf>", "ref")
                .replace("<p2>", MainActivity.secondaryPlayer)
                .replace("<t1>", MainActivity.attackingTeam)
                .replace("<t2>", MainActivity.defendingTeam)
                .replace("|", ". ")
                .replace("<R>", "\uD83D\uDFE5 ")
                .replace("<Y>", "\uD83D\uDFE8 ")
                .replace("<st>", MainActivity.venue)
                .replace("{s}", "'s");
    }

    public static class Content extends AsyncTask<Void, Void, Void> {
        public String url = ""; // "https://fulltime.thefa.com/displayTeam.html?divisionseason=986724292&teamID=803529560";
        public SelectionAdapter mCustomListAdapter = null;
        public static List<CustomList> teamList = null;
        public static EditText editTeamName = null;

        String title = "";
        String body = "";
        String[] club;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                //Connect to the website
                Document document = Jsoup.connect(url).get();

                //Get the title of the website
                title = document.title();
                Log.d("title", " = " + title);
                body = document.body().html();

                String tablea = "<td>";
                String tableb = "</td>";
                String addzero = "<td>\t\t\t\t\t\t\t\t\t\t\t\t</td>";
                String ware = "\">";

                String[] sentences = body.split("personID=");
                for (int i = 1; i < sentences.length; i++) { // ignore 0
                    sentences[i] = sentences[i].replaceAll(addzero, "<td>0</td>");
                    sentences[i] = sentences[i].replaceAll(tablea, "");
                    sentences[i] = sentences[i].replaceAll(tableb, "/");
                    sentences[i] = sentences[i].replaceAll("\t", "");
                    sentences[i] = sentences[i].replaceAll("</a> </th> ", "/");
                    sentences[i] = sentences[i].replaceAll(ware, "/");
                    sentences[i] = sentences[i].replaceAll(", ", "/");
                    sentences[i] = sentences[i].replaceAll("</a></th>", "/");
                    String[] squad = sentences[i].split("/");
                    CustomList what = new CustomList();
                    what.name = squad[2] + squad[1].trim();
            //        what.id = squad[0];
                    what.mobile = "0";
                    Log.d("what", " = " + what.name);
                    what.status = 3;
                    teamList.add(what);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            club = title.split("\\|");
            Log.d("club", " = " + club.toString());
            Log.d("club 0", " = " + club[0]);
            editTeamName.setText(club[0].trim());
            mCustomListAdapter.notifyDataSetChanged();
            super.onPostExecute(aVoid);
        }
    }
}
