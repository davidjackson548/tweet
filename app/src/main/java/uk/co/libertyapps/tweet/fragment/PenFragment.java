package uk.co.libertyapps.tweet.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.adapters.PenAdapter;

public class PenFragment extends DialogFragment implements PenAdapter.Listener {

    public static RecyclerView recyclerPendingList;
    public static RecyclerView recyclerView;
    TextView textPreviousDate, textEmptyList, textCurrentDate;
    Button editTeam;
    List<CustomList> teamList = new ArrayList<CustomList>();
    public static int team = 0;

    public static PenFragment newInstance() {
        return new PenFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_team, container, false);

        textEmptyList = view.findViewById(R.id.textEmptyList);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerPendingList = view.findViewById(R.id.recyclerPendingList);
        textPreviousDate = view.findViewById(R.id.textPreviousDate);
        textCurrentDate = view.findViewById(R.id.textCurrentDate);
        editTeam = view.findViewById(R.id.editTeam);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerPendingList.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerPendingList.setVisibility(View.GONE);
        editTeam.setVisibility(View.GONE);

        textCurrentDate.setText("Which player is taking penalty?");

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        onSuccess();
    }

    public void onSuccess() {
        try {
            if (teamList.size() > 0) {
                textEmptyList.setVisibility(View.GONE);
            } else {
                textEmptyList.setText("Team have no players");
                textEmptyList.setVisibility(View.VISIBLE);
            }

            PenAdapter mCustomListAdapter =
                    new PenAdapter(getActivity(), teamList, this);
            recyclerView.setAdapter(mCustomListAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setEmptyList(boolean visibility) {
        textEmptyList.setVisibility(visibility ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    public void setArguments(int arguments) {
        team = arguments;
        // get which team
        if (team == Constants.HOME_TEAM) {
            teamList = MainActivity.homeTeamList;
        }
        if (team == Constants.AWAY_TEAM) {
            teamList = MainActivity.awayTeamList;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}