package uk.co.libertyapps.tweet.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.ImageCreation;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.OptionActivity;
import uk.co.libertyapps.tweet.R;

public class ThirdFragment extends Fragment {
    static EditText editHashtag, editVenue;
    int tab = 99;

    public ThirdFragment() {

    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView fullTime = view.findViewById(R.id.fullTime);
        ImageView halfTime = view.findViewById(R.id.halfTime);
        ImageView teamGoal = view.findViewById(R.id.teamGoal);
        ImageView getStart = view.findViewById(R.id.startingLineup);
        SwitchCompat genderSwitch  = view.findViewById(R.id.genderSwitch);
        RadioGroup rg = (RadioGroup) view.findViewById(R.id.radioETP);

        Log.d("TF tab", String.valueOf(tab));
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("match", Context.MODE_PRIVATE);
        editHashtag = view.findViewById(R.id.editHashtag);
        editHashtag.setText(sharedpreferences.getString("hashtag", ""));
        editVenue = view.findViewById(R.id.editVenue);
        editVenue.setText(sharedpreferences.getString("editVenue", ""));

        Uri uri = Uri.parse(sharedpreferences.getString("editHomeGoal", ""));
        Log.d("uri", uri.toString());
        if (uri.toString().length() > 0) {
            Bitmap bitmap1 = BitmapFactory.decodeFile(String.valueOf(uri));
            teamGoal.setImageBitmap(bitmap1);
        }

        uri = Uri.parse(sharedpreferences.getString("editLineup", ""));
        Log.d("uri", uri.toString());
        if (uri.toString().length() > 0) {
            Bitmap bitmap1 = BitmapFactory.decodeFile(String.valueOf(uri));
            getStart.setImageBitmap(bitmap1);
        }

        Button options = view.findViewById(R.id.options);
        Button save = view.findViewById(R.id.saveOptions);
        Button deleteHome = view.findViewById(R.id.deleteHome);
        Button deleteAway = view.findViewById(R.id.deleteAway);

        deleteHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("home",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                FirstFragment.teamList.clear();
                Intent intent = new Intent(getActivity(), OptionActivity.class);
                intent.putExtra("Team", Constants.HOME_TEAM);
                startActivity(intent);
            }
        });

        deleteAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("deleteAway", "deleteAway");
                SharedPreferences preferences = getActivity().getSharedPreferences("away",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                SecondFragment.teamList.clear();
                Intent intent = new Intent(getActivity(), OptionActivity.class);
                intent.putExtra("Team", Constants.AWAY_TEAM);
                startActivity(intent);
            }
        });

        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("match",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(getActivity(), OptionActivity.class);
                startActivity(intent);

/*                Intent i = new Intent();
                i.setType("application/zip");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select ZIP"), OptionActivity.SELECT_ZIP );*/
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                SharedPreferences preferences = getActivity().getSharedPreferences("match",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("extra", checkedId);

                switch(checkedId){
                    case R.id.radioNo:
                        Log.d("onCheckedChanged", "R.id.radioNo");
                        editor.putInt("extra", 0);
                        break;
                    case R.id.radioExtra:
                        Log.d("onCheckedChanged", "R.id.radioExtra");
                        editor.putInt("extra", 1);
                        break;
                    case R.id.radioPens:
                        Log.d("onCheckedChanged", "R.id.radioPens");
                        editor.putInt("extra", 2);
                        break;
                }
                editor.apply();
            }
        });

        genderSwitch.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        SharedPreferences preferences = getActivity().getSharedPreferences("match",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("sex", isChecked);
                        editor.apply();
                    }
                });

        teamGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Open", "teamGoal");
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Goal"), OptionActivity.SELECT_GOAL_HOME);
            }
        });

        getStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Open", "getStart");
                // ImageCreation.drawTeamsheet(getActivity(), FirstFragment.teamList, "");
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Lineup"), OptionActivity.SELECT_LINEUP);
            }
        });

        fullTime.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d("Open", "LongClick");

                //todo // new alert dialog with imageview
                // 2 draggable?
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                View et = LayoutInflater.from(getActivity()).inflate(R.layout.image_mover, null,false);

          //      Bitmap back = BitmapFactory.decodeResource(getResources(), R.drawable.full_time);
         //       Bitmap home = BitmapFactory.decodeResource(getResources(), R.drawable.fylde);
         //       Bitmap away = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        //        et.setImageBitmap(back);
             //   et.setHint("New time (For example) 0:00");
                alertDialogBuilder.setView(et);

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return true;
            }
        });

        fullTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Open", "teamLogo");
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Full Time Image"), OptionActivity.SELECT_FULL_TIME);
            }
        });
        halfTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Open", "teamLogo");
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "Select Half Time Image"), OptionActivity.SELECT_HALF_TIME);
            }
        });

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("TF", "onSaveInstanceState");
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("match", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        if (editHashtag.getText().toString() != null) {
            editor.putString("hashtag", editHashtag.getText().toString());
        }

        if (editVenue.getText().toString() != null) {
            editor.putString("editVenue", editVenue.getText().toString());
        }
        editor.apply();
    }
}