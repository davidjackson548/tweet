package uk.co.libertyapps.tweet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DecompressFast {
    private String _zipFile;
    private String _location;
    private Context mContext;

    public DecompressFast(String zipFile, String location, Context context) {
        _zipFile = zipFile;
        _location = location;
        mContext = context;
        _dirChecker("");
    }

    public void unzip() {
        List<CustomList> customLists = Team.loadTeam(mContext, Constants.HOME_TEAM);
        int[] files = new int[5];
        int f = 0;
        try  {
            FileInputStream fin = new FileInputStream(_zipFile);
            ZipInputStream zin = new ZipInputStream(fin);
            ZipEntry ze = null;

            while ((ze = zin.getNextEntry()) != null) {
               // Log.v("Decompress", "Unzipping " + ze.getName());
                if(ze.isDirectory()) {
                    _dirChecker(ze.getName());
                } else {
               //     Log.v("_location + ze.getName", _location + ze.getName());
//                    files[f] = Integer.parseInt(ze.getName().replaceAll("([^0-9])", ""));
                    f++;

                    File destinationFilename = new File(mContext.getFilesDir().getPath() + File.separatorChar + ze.getName());


                    OptionActivity.createFileFromStream(zin, destinationFilename);
                    FileOutputStream fout = new FileOutputStream(_location + ze.getName());
                    BufferedOutputStream bufout = new BufferedOutputStream(fout);

                    byte[] buffer = new byte[1024];
                    int read = 0;
                    while ((read = zin.read(buffer)) != -1) {
                        bufout.write(buffer, 0, read);
                    }
                    bufout.close();
                    zin.closeEntry();
                    fout.close();

                }
            }
            zin.close();
         //   Log.d("Unzip", "Unzipping complete. path :  " +_location );

        } catch(Exception e) {
            Log.e("Decompress", "unzip", e);
            Log.d("Unzip", "Unzipping failed");
        }

        Log.d("files", String.valueOf(files.toString()));

     /*   //todo compare vs all mobile
        for (int file : files) {
            Log.v("file", String.valueOf(file));
            for (int i = 0; i < customLists.size(); i++) {
              //  Log.v("get(i).mobile", customLists.get(i).mobile);
                if (Integer.parseInt(customLists.get(i).mobile) == file) {
                    Log.v("match", String.valueOf(file));

                    customLists.get(i).twitter = _location + file + ".png";

                    //todo get uri for unzip image
                    //todo save image to player
                }
            }
        }
        Log.d("customLists.toString())", customLists.toString());

        SharedPreferences sharedh = mContext.getSharedPreferences("home", Context.MODE_PRIVATE);
        sharedh.edit().putString("editHomeList", customLists.toString()).apply();*/
        Intent intent = new Intent(mContext, MainActivity.class);
        mContext.startActivity(intent);

    }

    private void _dirChecker(String dir) {
        Log.d("dirChecker", dir);
        File f = new File(_location + dir);
        if(!f.isDirectory()) {
            f.mkdirs();
        }
    }
}
