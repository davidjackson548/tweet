package uk.co.libertyapps.tweet;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CustomList implements Serializable {

    public static final long serialID = 1234L;

    public String id;
    public String name;
    public String twitter;
    public String mobile;
//    public String image;
    public int status;

    /**
     * No args constructor for use in serialization
     */
    public CustomList() {
    }

    /**
     * @param id
     * @param twitter
     * @param name
     * @param mobile
     */
    public CustomList(String id, String name, String twitter, String mobile, int status) {
        this.id = id;
        this.name = name;
        this.twitter = twitter;
        this.mobile = mobile;
        this.status = status;
 //       this.image = image;
    }

    public int getNumber() {
        if (mobile.equals("")) {
            mobile = "0";
        }
        return Integer.parseInt(mobile);
    }

    //todo
    public String getImage() {
        return "/storage/emulated/0/Android/data/uk.co.libertyapps.tweet/files/new.gif";
    }

    public int getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

   @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public static void order(List<CustomList> persons) {

        Collections.sort(persons, new Comparator() {

            public int compare(Object o1, Object o2) {

                Integer x1 = ((CustomList) o1).getStatus();
                Integer x2 = ((CustomList) o2).getStatus();
                int sComp = x1.compareTo(x2);

                if (sComp != 0) {
                    return sComp;
                }

                Integer x3 = ((CustomList) o1).getNumber();
                Integer x4 = ((CustomList) o2).getNumber();

                int nComp = x3.compareTo(x4);

                if (nComp != 0) {
                    return nComp;
                }

                String x5 = ((CustomList) o1).getName();
                String x6 = ((CustomList) o2).getName();
                int tComp = x5.compareTo(x6);
                if (tComp != 0) {
                    return tComp;
                }

                return 0;
            }
        });
    }

    //this order is squad number / name (not status)
    public static void orderForSelection(List<CustomList> persons) {

        Collections.sort(persons, new Comparator() {

            public int compare(Object o1, Object o2) {

                Integer x3 = ((CustomList) o1).getNumber();
                Integer x4 = ((CustomList) o2).getNumber();

                int nComp = x3.compareTo(x4);

                if (nComp != 0) {
                    return nComp;
                }

                String x5 = ((CustomList) o1).getName();
                String x6 = ((CustomList) o2).getName();
                int tComp = x5.compareTo(x6);
                if (tComp != 0) {
                    return tComp;
                }

                return 0;
            }
        });
    }
}
