package uk.co.libertyapps.tweet.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uk.co.libertyapps.tweet.Constants;
import uk.co.libertyapps.tweet.CustomList;
import uk.co.libertyapps.tweet.MainActivity;
import uk.co.libertyapps.tweet.R;
import uk.co.libertyapps.tweet.fragment.CommentaryFragment;
import uk.co.libertyapps.tweet.fragment.HomeFragment;
import uk.co.libertyapps.tweet.fragment.PlayerFragment;
import uk.co.libertyapps.tweet.sugar.Commentary;

public class StartingAdapter extends RecyclerView.Adapter<StartingAdapter.CustomListViewHolder> {
    Activity mContext;
    List<CustomList> mCustomList;
    Listener mListener;
    int team, selected;

    public StartingAdapter(Activity context, List<CustomList> customList,
                           Listener listener, int team, int selected) {
        this.mCustomList = customList;
        this.mContext = context;
        this.mListener = listener;
        this.team = team;
        this.selected = selected;
    }

    @Override
    public CustomListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_row, parent, false);
        return new CustomListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomListViewHolder holder, int position) {
        CustomList customList = mCustomList.get(position);
        holder.textName.setText(customList.name);
        holder.textMobile.setText(customList.mobile);

        holder.cardView.setOnClickListener(v -> {
        //    MainActivity.mViewPager.setCurrentItem(Constants.COMMENTARY_PAGER_ID);
            MainActivity.activePlayer = customList.name;

            // open frag here
            CommentaryFragment cf = new CommentaryFragment();

            FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.setReorderingAllowed(true);
            transaction.replace(R.id.eventFrag, cf, null);
            transaction.commit();

            cf.setArguments(Integer.parseInt(MainActivity.search));

       //     cf.spinner.setSelection(Integer.parseInt(MainActivity.search), false);

            MainActivity.matchReport += MainActivity.minutes  + " " + Constants.EVENT_ITEMS[Integer.parseInt(MainActivity.search)] + " " + MainActivity.activePlayer + " " + MainActivity.attackingTeam + " " + "\n";
            Log.d("MatchReport", MainActivity.matchReport);
        });


        int start = 0;
        int bench = 0;
        String TeamName = "";

        if (team == Constants.HOME_TEAM) {
            start = MainActivity.homeStart;
            bench = MainActivity.homeBench;
            TeamName = MainActivity.homeTeamName;

        }
        if (team == Constants.AWAY_TEAM) {
            start = MainActivity.awayStart;
            bench = MainActivity.awayBench;
            TeamName = MainActivity.awayTeamName;
        }

        if (position == (start - 1)) {
            // put sub menu here
            View view = LayoutInflater.from(mContext).inflate(R.layout.list_sub, holder.cardView,false);

            TextView SubTitle = view.findViewById(R.id.SubTitle);
            SubTitle.setText(TeamName + " Subs");

            view.setOnClickListener(null);

            ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(view.getLayoutParams());
            marginLayoutParams.setMargins(0, 200, 0, 0);
            view.setLayoutParams(marginLayoutParams);

            holder.cardView.addView(view);
        }

        if (position == ((start + bench) - 1)) {
            // put sub menu here
            View view = LayoutInflater.from(mContext).inflate(R.layout.list_sub, holder.cardView,false);
          //  Log.d("SA position", String.valueOf(position));

            TextView SubTitle = view.findViewById(R.id.SubTitle);
            SubTitle.setText(TeamName + " Reserves");

            view.setOnClickListener(null);

            ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(view.getLayoutParams());
            marginLayoutParams.setMargins(0, 200, 0, 0);
            view.setLayoutParams(marginLayoutParams);

            holder.cardView.addView(view);
        }
    }

    @Override
    public int getItemCount() {
        return mCustomList.size();
    }

    public List<CustomList> getCustomList() {
        return mCustomList;
    }

    public void updateCustomList(List<CustomList> customList) {
        this.mCustomList = customList;
    }

    public interface Listener {
        void setEmptyList(boolean visibility);
    }

    public class CustomListViewHolder extends RecyclerView.ViewHolder {
        TextView textName;
        TextView textMobile;
        CardView cardView;

        public CustomListViewHolder(View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.textName);
            textMobile = itemView.findViewById(R.id.textMobile);
            cardView = itemView.findViewById(R.id.cardView);
        }

    }
}
